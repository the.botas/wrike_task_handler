/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database;

import com.intellij.openapi.util.Pair;
import host.Identity;
import org.ini4j.Ini;
import org.ini4j.IniPreferences;
import org.junit.Test;
import utils.Result;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.*;

public class TaskControllerTest {

    @Test
    public void newController() {
        try {
            new TaskController(null);
        } catch (Exception error) {
            fail("call to new TaskController(null) generated exception "+error.toString());
        }

        TaskController testedObject = new TaskController(null);
        assertTrue(testedObject.createTaskFromString("some","").isError());
        assertFalse(testedObject.closeTask("some").isEmpty());

        assertTrue(new TaskController(new Identity()).createTaskFromString("some","").isError());
        assertFalse(new TaskController(new Identity()).closeTask("some").isEmpty());
    }


    @Test
    public void manageTask() {
        TaskController invalidDatabase = new TaskController(new Identity());
        String taskText = "test"+new Random().nextInt(31999);
        assertTrue(invalidDatabase.createTaskFromString(taskText, null).isError());
        assertFalse(invalidDatabase.closeTask(taskText).isEmpty());

        TaskController validDatabase = setUpValidBackend();
        Result<Pair<String,String>> taskData = validDatabase.createTaskFromString(taskText, null);
        assertTrue(taskData.isOk());
        assertTrue(taskData.getOk().getSecond().contains("WrikeID_"));
        assertNull(validDatabase.closeTask(taskData.getOk().getFirst()+" "+taskData.getOk().getSecond()));
    }

    @Test
    public void invalidCreateTaskParameters() {
        TaskController validDatabase = setUpValidBackend();
        assertTrue(validDatabase.createTaskFromString(null, null).isError());
        assertTrue(validDatabase.createTaskFromString("", null).isError());
    }

    @Test
    public void invalidCloseTaskParameters() {
        TaskController validDatabase = setUpValidBackend();
        assertFalse(validDatabase.closeTask(null).isEmpty());
        assertFalse(validDatabase.closeTask("").isEmpty());
    }

    private TaskController setUpValidBackend() {
        Ini ini = null;
        try {
            ini = new Ini(new File("src/test/resources/testData.txt"));
        } catch (Exception error) {
            fail(error.toString());
        }
        assertNotNull(ini);
        java.util.prefs.Preferences settings = new IniPreferences(ini);
        Identity identity = new Identity();
        identity.setToken(settings.node("connection").get("pass", null));
        identity.setProject(settings.node("connection").get("project", null));
        return new TaskController(identity);
    }
}