/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.Models;

import com.google.gson.JsonObject;
import org.junit.Test;

import static org.junit.Assert.*;

public class WrikeUserTest {

    @Test
    public void fromJsonObject() {
        WrikeUser testedObject = new WrikeUser();

        assertFalse(testedObject.fromJsonObject(new JsonObject()).isEmpty());
        assertFalse(testedObject.fromJsonObject(null).isEmpty());

        JsonObject testData = new JsonObject();
        testData.addProperty(WrikeUser.JsonField.me, WrikeUser.JsonField.isMe);
        assertFalse(testedObject.fromJsonObject(testData).isEmpty());

        testData.addProperty(WrikeUser.JsonField.lastName, "Some");
        assertFalse(testedObject.fromJsonObject(testData).isEmpty());

        testData.addProperty(WrikeUser.JsonField.firstName, "Name");
        assertFalse(testedObject.fromJsonObject(testData).isEmpty());

        testData.addProperty(WrikeUser.JsonField.uid, "IDvalue");
        assertFalse(testedObject.fromJsonObject(testData).isEmpty());

        testData.addProperty(WrikeUser.JsonField.type, "someType");
        assertNull(testedObject.fromJsonObject(testData));
        assertEquals("",testedObject.getId());

        testData.addProperty(WrikeUser.JsonField.type, WrikeUser.JsonField.personType);
        assertNull(testedObject.fromJsonObject(testData));
        assertEquals("Name Some", testedObject.getName());
        assertEquals("IDvalue", testedObject.getId());
        assertTrue(testedObject.isMe());
    }
}