/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.Models;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.junit.Test;

import static org.junit.Assert.*;

public class WrikeProjectTest {

    @Test
    public void fromJsonObject() {
        WrikeProject testedObject = new WrikeProject();

        testedObject.fromJsonObject(new JsonObject());
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());

        JsonObject sourceData = new GsonBuilder().create().fromJson("{id:123456}", JsonObject.class);
        assertFalse(testedObject.fromJsonObject(sourceData).isEmpty());
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());


        sourceData = new GsonBuilder().create().fromJson("{scope: some, id:123456}", JsonObject.class);
        assertNull(testedObject.fromJsonObject(sourceData));
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());

        sourceData = new GsonBuilder().create().fromJson("{scope: WsFolder, id:123456}", JsonObject.class);
        assertNull(testedObject.fromJsonObject(sourceData));
        assertEquals("123456",testedObject.getId());
        assertEquals("",testedObject.getTitle());

        sourceData = new GsonBuilder().create().fromJson("{id:\"\",title=\"\"}", JsonObject.class);
        assertFalse(testedObject.fromJsonObject(sourceData).isEmpty());
        assertEquals("123456",testedObject.getId());
        assertEquals("",testedObject.getTitle());

        sourceData = new GsonBuilder().create().fromJson("{scope: WsFolder, id:\"\",title=\"\"}", JsonObject.class);
        assertNull(testedObject.fromJsonObject(sourceData));
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());

        sourceData = new GsonBuilder().create().fromJson("{title=SomeTitle}", JsonObject.class);
        assertFalse(testedObject.fromJsonObject(sourceData).isEmpty());
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());

        sourceData = new GsonBuilder().create().fromJson("{scope: WsFolder, title=SomeTitle}", JsonObject.class);
        assertFalse(testedObject.fromJsonObject(sourceData).isEmpty());
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());

        sourceData = new GsonBuilder().create().fromJson("{scope: WsFolder, id:789,title=NewTitle}", JsonObject.class);
        assertNull(testedObject.fromJsonObject(sourceData));
        assertEquals("789",testedObject.getId());
        assertEquals("NewTitle",testedObject.getTitle());

        assertFalse(testedObject.fromJsonObject(null).isEmpty());
    }

    @Test
    public void fromJsonObjectIgnoresInvalidValues() {
        WrikeProject testedObject = new WrikeProject();

        JsonObject sourceData = new GsonBuilder().create().fromJson("{scope: WsFolder, id:789,title=NewTitle}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("789",testedObject.getId());
        assertEquals("NewTitle",testedObject.getTitle());

        testedObject.fromJsonObject(new JsonObject());
        assertEquals("789",testedObject.getId());
        assertEquals("NewTitle",testedObject.getTitle());

        sourceData = new GsonBuilder().create().fromJson("{id:123456}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("789",testedObject.getId());
        assertEquals("NewTitle",testedObject.getTitle());


        sourceData = new GsonBuilder().create().fromJson("{title=SomeTitle}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("789",testedObject.getId());
        assertEquals("NewTitle",testedObject.getTitle());

    }
}