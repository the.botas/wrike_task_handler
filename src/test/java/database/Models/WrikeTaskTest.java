/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.Models;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.junit.Test;
import utils.StringOperations;

import static org.junit.Assert.*;

public class WrikeTaskTest {

    @Test
    public void testSplitByNonAlfaNumericChars() {
        String[] expected = new String[]{"Here", "is", "an", "ex", "mple"};
        String[] test = "Here is an ex@mple".split("\\P{LD}+");
        assertArrayEquals(  expected, test);
    }

    @Test
    public void addIdFromDelimitedText() {
        WrikeTask testedObject = new WrikeTask();

        String sourceData = "some text with no ID";
        assertNotNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals("",testedObject.getId());

        String uid = "1";
        sourceData = WrikeTask.uidDelimiter+uid+" some text with no ID";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "12";
        sourceData = " \t"+ WrikeTask.uidDelimiter+uid+" some text with no ID";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "123";
        sourceData = " \t some "+ WrikeTask.uidDelimiter+uid+" text with no ID";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "1234";
        sourceData = " \t some text with no ID "+ WrikeTask.uidDelimiter+uid;
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "12345";
        sourceData = " \t some text with no ID"+
                StringOperations.newLineDelimiter + WrikeTask.uidDelimiter+uid;
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "123456";
        sourceData = StringOperations.newLineDelimiter+ WrikeTask.uidDelimiter+uid+
                    StringOperations.newLineDelimiter+
                    " \t some text with no ID";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "CZTWVKQLMHYD";
        sourceData = WrikeTask.uidDelimiter+uid+"\");";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "CZTWVKQLMHYE";
        sourceData = WrikeTask.uidDelimiter+uid+";";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "CZTWVKQLMHYF";
        sourceData = WrikeTask.uidDelimiter+uid+"]";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        uid = "CZTWVKQLMHYG";
        sourceData = WrikeTask.uidDelimiter+uid+"}";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        sourceData = WrikeTask.uidDelimiter+uid+"}"+WrikeTask.uidDelimiter;
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        sourceData = WrikeTask.uidDelimiter+uid+"}"+WrikeTask.uidDelimiter+" ";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        sourceData = WrikeTask.uidDelimiter+uid+"}"+WrikeTask.uidDelimiter+"\"";
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        sourceData = WrikeTask.uidDelimiter+uid+"}"+WrikeTask.uidDelimiter+uid;
        assertNotNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        sourceData = WrikeTask.uidDelimiter+uid+"}"+StringOperations.newLineDelimiter+WrikeTask.uidDelimiter+uid;
        assertNotNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        sourceData = WrikeTask.uidDelimiter+uid+"}"+StringOperations.newLineDelimiter+WrikeTask.uidDelimiter;
        assertNull(testedObject.addIdFromDelimitedText(sourceData));
        assertEquals(uid,testedObject.getId());

        assertFalse(testedObject.addIdFromDelimitedText(null).isEmpty());
    }

    @Test
    public void addTitle() {
        WrikeTask testedObject = new WrikeTask();

        assertFalse(testedObject.addTitle("").isEmpty());
        assertEquals("", testedObject.getTitle());
        assertFalse(testedObject.addTitle(null).isEmpty());
        assertEquals("", testedObject.getTitle());
        assertFalse(testedObject.addTitle(" \t").isEmpty());
        assertEquals("", testedObject.getTitle());
        assertFalse(testedObject.addTitle(StringOperations.newLineDelimiter).isEmpty());
        assertEquals("", testedObject.getTitle());

        String title = "SomeTitle";
        assertNull(testedObject.addTitle(" \t"+StringOperations.newLineDelimiter+title+" \t"+StringOperations.newLineDelimiter));
        assertEquals(title, testedObject.getTitle());
    }

    @Test
    public void setStatus() {
        WrikeTask testedObject = new WrikeTask();

        assertFalse(testedObject.setStatus("").isEmpty());
        assertEquals("", testedObject.getStatus());
        assertFalse(testedObject.setStatus(null).isEmpty());
        assertEquals("", testedObject.getStatus());
        assertFalse(testedObject.setStatus(" \t").isEmpty());
        assertEquals("", testedObject.getStatus());
        assertFalse(testedObject.setStatus(StringOperations.newLineDelimiter).isEmpty());
        assertEquals("", testedObject.getStatus());

        assertNull(testedObject.setStatus(WrikeTask.Status.active));
        assertEquals(WrikeTask.Status.active, testedObject.getStatus());
        assertNull(testedObject.setStatus(WrikeTask.Status.completed));
        assertEquals(WrikeTask.Status.completed, testedObject.getStatus());
        assertNull(testedObject.setStatus(WrikeTask.Status.deferred));
        assertEquals(WrikeTask.Status.deferred, testedObject.getStatus());
        assertNull(testedObject.setStatus(WrikeTask.Status.canceled));
        assertEquals(WrikeTask.Status.canceled, testedObject.getStatus());
    }

    @Test
    public void fromJsonObject() {
        WrikeTask testedObject = new WrikeTask();

        assertFalse(testedObject.fromJsonObject(null).isEmpty());

        assertFalse(testedObject.fromJsonObject(new JsonObject()).isEmpty());

        JsonObject sourceData = new GsonBuilder().create().fromJson("{data:null}", JsonObject.class);
        assertFalse(testedObject.fromJsonObject(sourceData).isEmpty());

        sourceData = new GsonBuilder().create().fromJson("{data:123}", JsonObject.class);
        assertFalse(testedObject.fromJsonObject(sourceData).isEmpty());

        sourceData = new GsonBuilder().create().fromJson("{data:[]}", JsonObject.class);
        assertFalse(testedObject.fromJsonObject(sourceData).isEmpty());

        sourceData = new GsonBuilder().create().fromJson("{data:[{}]}", JsonObject.class);
        assertNull(testedObject.fromJsonObject(sourceData));
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());
        assertEquals("",testedObject.getDescription());
        assertEquals("",testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        sourceData = new GsonBuilder().create().fromJson("{data:[{id:1234}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("1234",testedObject.getId());
        assertEquals("",testedObject.getTitle());
        assertEquals("",testedObject.getDescription());
        assertEquals("",testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        sourceData = new GsonBuilder().create().fromJson("{data:[{title:someTitle}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("1234",testedObject.getId());
        assertEquals("someTitle",testedObject.getTitle());
        assertEquals("",testedObject.getDescription());
        assertEquals("",testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        sourceData = new GsonBuilder().create().fromJson("{data:[{description:someDescription}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("1234",testedObject.getId());
        assertEquals("someTitle",testedObject.getTitle());
        assertEquals("someDescription",testedObject.getDescription());
        assertEquals("",testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        sourceData = new GsonBuilder().create().fromJson("{data:[{status:someStatus}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("1234",testedObject.getId());
        assertEquals("someTitle",testedObject.getTitle());
        assertEquals("someDescription",testedObject.getDescription());
        assertEquals("",testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        sourceData = new GsonBuilder().create().fromJson("{data:[{status:Active}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("1234",testedObject.getId());
        assertEquals("someTitle",testedObject.getTitle());
        assertEquals("someDescription",testedObject.getDescription());
        assertEquals(WrikeTask.Status.active,testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        sourceData = new GsonBuilder().create().
                fromJson("{data:[" +
                        "{id:\"\",title:\"\",description:\"\",status:invalid}," +
                        "{id:aaa,title:aaa,description:aaa,status:Deferred}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());
        assertEquals("",testedObject.getDescription());
        assertEquals(WrikeTask.Status.active,testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        String description = "some<br>new line<br>Description";
        sourceData = new GsonBuilder().create().fromJson("{data:[{description:\"some<br>new line<br>Description\"}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());
        assertEquals(description.replace(StringOperations.htmlLineBreak,StringOperations.newLineDelimiter),testedObject.getDescription());
        assertEquals(WrikeTask.Status.active,testedObject.getStatus());
        assertEquals("", testedObject.getResponsible());

        sourceData = new GsonBuilder().create().fromJson("{data:[{responsibleIds: [ ID1, ID2 ]}]}", JsonObject.class);
        testedObject.fromJsonObject(sourceData);
        assertEquals("",testedObject.getId());
        assertEquals("",testedObject.getTitle());
        assertEquals(description.replace(StringOperations.htmlLineBreak,StringOperations.newLineDelimiter),testedObject.getDescription());
        assertEquals(WrikeTask.Status.active,testedObject.getStatus());
        assertEquals("ID1", testedObject.getResponsible());

    }

    @Test
    public void fromText() {
        WrikeTask testedObject = new WrikeTask();

        String text = "some text";
        assertNull(testedObject.fromText(text));
        assertEquals(text,testedObject.getTitle());
        assertEquals(text,testedObject.getDescription());


        String testSource = StringOperations.commentMarker+text;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = StringOperations.newLineDelimiter+text;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+text;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = " "+text;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = "\t"+text;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = text+StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = StringOperations.commentMarker+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = StringOperations.newLineDelimiter+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = " "+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = "\t"+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        testSource = " \t"+StringOperations.commentMarker+text;
        assertNull(testedObject.fromText(testSource));
        assertEquals(text,testedObject.getTitle());
        assertEquals(testSource,testedObject.getDescription());

        assertNotNull(testedObject.fromText(""));
        assertEquals(text, testedObject.getTitle());
        assertEquals(testSource, testedObject.getDescription());

        text = "some";
        assertNull(testedObject.fromText(text));
        assertNotNull(testedObject.fromText(null));
        assertEquals(text,testedObject.getTitle());
        assertEquals(text,testedObject.getDescription());

        assertNotNull(testedObject.fromText(WrikeTask.uidDelimiter+"some"));
        assertNotNull(testedObject.fromText("some"+StringOperations.newLineDelimiter+WrikeTask.uidDelimiter+"some"));
        assertNull(testedObject.fromText("some"+StringOperations.newLineDelimiter+WrikeTask.uidDelimiter));
    }

    @Test
    public void getNoteWithId() {
        WrikeTask testedObject = new WrikeTask();

        assertEquals(" "+ WrikeTask.uidDelimiter+" ",testedObject.getNoteWithId());

        String id = "123456";
        testedObject.addIdFromDelimitedText(WrikeTask.uidDelimiter+id);

        assertEquals(" "+ WrikeTask.uidDelimiter+id+" ",testedObject.getNoteWithId());
    }

    @Test
    public void asParameterString() {
        WrikeTask testedObect = new WrikeTask();

        assertEquals("",testedObect.asParameterString());

        String title = "some title";
        testedObect.addTitle(title);

        assertEquals("title="+StringOperations.replaceUrlEscapeChars(title).getOk(),testedObect.asParameterString());

        String description = title+StringOperations.newLineDelimiter+
                StringOperations.commentMarker+" some new line";
        testedObect.fromText(description);
        assertEquals("title="+StringOperations.replaceUrlEscapeChars(title).getOk()+
                "&description="+StringOperations.replaceUrlEscapeChars(description.replace(StringOperations.newLineDelimiter,StringOperations.htmlLineBreak)).getOk(),
                testedObect.asParameterString());

        testedObect.setStatus(WrikeTask.Status.deferred);
        assertEquals("title="+StringOperations.replaceUrlEscapeChars(title).getOk()+
                "&description="+StringOperations.replaceUrlEscapeChars(description.replace(StringOperations.newLineDelimiter,StringOperations.htmlLineBreak)).getOk()+
                "&status="+ WrikeTask.Status.deferred,
                testedObect.asParameterString());

        String responsible = "responsible";
        testedObect.setResponsible(responsible);
        assertEquals("title="+StringOperations.replaceUrlEscapeChars(title).getOk()+
                        "&description="+StringOperations.replaceUrlEscapeChars(description.replace(StringOperations.newLineDelimiter,StringOperations.htmlLineBreak)).getOk()+
                        "&status="+ WrikeTask.Status.deferred+
                        "&responsibles=[\""+responsible+"\"]",
                testedObect.asParameterString());

    }

    @Test
    public void statusAsParameterString() {
        WrikeTask testedObect = new WrikeTask();
        String statusTag = "status=";
        String parameterDelimiter = "&";

        assertEquals(statusTag, testedObect.statusAsParameterString(""));

        assertEquals(statusTag, testedObect.statusAsParameterString(null));

        String initalParameters = " \t";
        assertEquals(initalParameters+parameterDelimiter+statusTag, testedObect.statusAsParameterString(initalParameters));

        testedObect.setStatus(WrikeTask.Status.canceled);
        assertEquals(initalParameters+parameterDelimiter+statusTag+ WrikeTask.Status.canceled,
                testedObect.statusAsParameterString(initalParameters));

    }

    @Test
    public void setResponsible() {
        WrikeTask testedObject = new WrikeTask();

        assertFalse(testedObject.setResponsible(null).isEmpty());
        assertEquals("", testedObject.getResponsible());
        assertFalse(testedObject.setResponsible("").isEmpty());
        assertEquals("", testedObject.getResponsible());
        assertFalse(testedObject.setResponsible(" \t\n").isEmpty());
        assertEquals("", testedObject.getResponsible());
        String responsible = "Someone";
        assertNull(testedObject.setResponsible(responsible));
        assertEquals(responsible, testedObject.getResponsible());

    }
}