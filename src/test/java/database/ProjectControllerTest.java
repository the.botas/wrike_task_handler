/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database;

import host.Identity;
import org.ini4j.Ini;
import org.ini4j.IniPreferences;
import org.junit.Test;
import utils.Result;

import java.io.File;
import java.util.HashMap;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class ProjectControllerTest {

    @Test
    public void newController() {
        try {
            new ProjectController(null);
        } catch (Exception error) {
            fail("call to new ProjectController(null) generated exception "+error.toString());
        }

        ProjectController testedObject = new ProjectController(null);
        assertTrue(testedObject.getProjects().isError());

        assertTrue(new ProjectController(new Identity()).getProjects().isError());
    }

    @Test
    public void getProjects() {
        Result<HashMap<String,String>> result = setUpValidBackend().getProjects();
        assertTrue(result.isOk());
        assertTrue(result.getOk().size()>0);
    }

    private ProjectController setUpValidBackend() {
        Ini ini = null;
        try {
            ini = new Ini(new File("src/test/resources/testData.txt"));
        } catch (Exception error) {
            fail(error.toString());
        }
        assertNotNull(ini);
        java.util.prefs.Preferences settings = new IniPreferences(ini);
        Identity identity = new Identity();
        identity.setToken(settings.node("connection").get("pass", null));
        identity.setProject(settings.node("connection").get("project", null));
        return new ProjectController(identity);
    }
}