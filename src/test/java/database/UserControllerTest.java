/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database;

import host.Identity;
import org.ini4j.Ini;
import org.ini4j.IniPreferences;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class UserControllerTest {

    @Test
    public void newController() {
        try {
            new UserController(null);
        } catch (Exception error) {
            fail("call to new UserController(null) generated exception "+error.toString());
        }

        UserController testedObject = new UserController(null);
        assertTrue(testedObject.getMyDatabaseId().isError());
        assertTrue(testedObject.getUsers().isError());

        assertTrue(new UserController(new Identity()).getMyDatabaseId().isError());
        assertTrue(new UserController(new Identity()).getUsers().isError());
    }

    @Test
    public void getMyDatabaseId() {
        UserController invalidDatabase = new UserController(new Identity());
        assertTrue(invalidDatabase.getMyDatabaseId().isError());

        UserController validDatabase = setUpValidBackend();
        assertTrue(validDatabase.getMyDatabaseId().isOk());
    }

    @Test
    public void getUsers() {
        assertTrue(new UserController(new Identity()).getUsers().isError());
        assertTrue(setUpValidBackend().getUsers().getOk().size()>0);
    }

    private UserController setUpValidBackend() {
        Ini ini = null;
        try {
            ini = new Ini(new File("src/test/resources/testData.txt"));
        } catch (Exception error) {
            fail(error.toString());
        }
        assertNotNull(ini);
        java.util.prefs.Preferences settings = new IniPreferences(ini);
        Identity identity = new Identity();
        identity.setToken(settings.node("connection").get("pass", null));
        identity.setProject(settings.node("connection").get("project", null));
        return new UserController(identity);
    }
}