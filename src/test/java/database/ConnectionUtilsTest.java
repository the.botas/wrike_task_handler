/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database;

import com.intellij.openapi.util.Pair;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConnectionUtilsTest {

    @Test
    public void baseAddress() {
        assertFalse(ConnectionUtils.baseAddress().isEmpty());
    }

    @Test
    public void getAuthorizationProperty() {
        Pair<String,String> result = ConnectionUtils.getAuthorizationProperty(null);
        assertFalse(result.getFirst().isEmpty());
        assertFalse(result.getSecond().isEmpty());

        String token = "some";
        result = ConnectionUtils.getAuthorizationProperty(token);
        assertFalse(result.getFirst().isEmpty());
        assertTrue(result.getSecond().endsWith(token));
    }
}