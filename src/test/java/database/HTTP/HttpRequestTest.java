/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.HTTP;

import com.intellij.openapi.util.Pair;
import org.junit.Test;
import utils.Result;

import javax.net.ssl.HttpsURLConnection;

import static org.junit.Assert.*;

public class HttpRequestTest {

    @Test
    public void getResponse() {
        DummyClass testedObject = new DummyClass();
        assertTrue(testedObject.getResponse(null,"some").isError());
        assertTrue(testedObject.getResponse("",null).isError());
    }

    class DummyClass extends HttpRequest {

        @Override
        protected String baseAddress() {
            return null;
        }

        @Override
        protected Pair<String, String> getAuthorizationProperty() {
            return null;
        }

        @Override
        protected void addParameters(HttpsURLConnection connection, String parameters) throws Exception {

        }

        @Override
        protected void setMethod(HttpsURLConnection connection) throws Exception {

        }

        public Result<Pair<Integer,String>> getResponse (String path, String parameters) {
            return super.getResponse(path,parameters);
        }
    }
}