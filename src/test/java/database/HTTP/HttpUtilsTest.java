/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.HTTP;

import com.google.gson.JsonObject;
import com.intellij.openapi.util.Pair;
import org.junit.Test;
import utils.Result;

import javax.net.ssl.HttpsURLConnection;

import static org.junit.Assert.*;

public class HttpUtilsTest {

    @Test
    public void httpResponseToJsonObject() {
        String member = "field";
        String value = "value";
        String validJson = "{"+member+": "+value+"}";
        JsonObject result = HttpUtils.httpResponseToJsonObject(validJson);
        assertNotNull(result);
        assertTrue(result.has(member));
        assertEquals("\"value\"",result.get(member).toString());

        assertNull(HttpUtils.httpResponseToJsonObject("some text"));
        assertNull(HttpUtils.httpResponseToJsonObject(""));
        assertNull(HttpUtils.httpResponseToJsonObject(null));
    }

    @Test
    public void getResponse() {
        HttpStub request = new HttpStub();
        assertEquals("", HttpUtils.getResponse(request,request.response200,"").getOk());
        assertEquals("", HttpUtils.getResponse(request,request.response399,"").getOk());
        assertTrue(HttpUtils.getResponse(request,request.response199,"").isError());
        assertTrue(HttpUtils.getResponse(request,request.response400,"").isError());
        assertTrue(HttpUtils.getResponse(request,request.throwException,"").isError());
        assertTrue(HttpUtils.getResponse(request,null,"").isError());
        assertTrue(HttpUtils.getResponse(request,"",null).isError());
        assertTrue(HttpUtils.getResponse(null,"","").isError());
    }

    @Test
    public void getDataArrayFromHttpResponse() {
        assertTrue(HttpUtils.getDataArrayFromHttpResponse(null).isError());
        assertTrue(HttpUtils.getDataArrayFromHttpResponse("").isError());
        assertTrue(HttpUtils.getDataArrayFromHttpResponse("{}").isError());
        assertTrue(HttpUtils.getDataArrayFromHttpResponse("{data}").isError());
        assertTrue(HttpUtils.getDataArrayFromHttpResponse("{data:some}").isError());
        assertTrue(HttpUtils.getDataArrayFromHttpResponse("{data:{}}").isError());
        assertEquals(0, HttpUtils.getDataArrayFromHttpResponse("{data:[]}").getOk().size());
        assertEquals(1, HttpUtils.getDataArrayFromHttpResponse("{data:[{}]}").getOk().size());
        assertEquals(2, HttpUtils.getDataArrayFromHttpResponse("{data:[{},{}]}").getOk().size());
    }


    class HttpStub extends HttpRequest {

        final String throwException = "throwException";
        final String response199 = "response199";
        final String response200 = "response200";
        final String response399 = "response399";
        final String response400 = "response400";

        @Override
        protected String baseAddress() {
            return null;
        }

        @Override
        protected Pair<String, String> getAuthorizationProperty() {
            return null;
        }

        @Override
        protected void addParameters(HttpsURLConnection connection, String parameters) throws Exception {
        }

        @Override
        protected void setMethod(HttpsURLConnection connection) throws Exception {
        }

        public Result<Pair<Integer,String>> getResponse (String path, String parameters) {
            if (null==path)
                return new Result<Pair<Integer,String>>().setError("null input error");

            if (path.isEmpty())
                return new Result<Pair<Integer,String>>().setError("empty input error");

            if (null==parameters)
                return new Result<Pair<Integer,String>>().setError("null input error");

            if (path.equals(response199))
                return new Result<Pair<Integer,String>>().setOk(new Pair<>(199,""));

            if (path.equals(response200))
                return new Result<Pair<Integer,String>>().setOk(new Pair<>(200,""));

            if (path.equals(response399))
                return new Result<Pair<Integer,String>>().setOk(new Pair<>(399,""));

            if (path.equals(response400))
                return new Result<Pair<Integer,String>>().setOk(new Pair<>(400,""));

            return null;
        }
    }
}