/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.HTTP;

import com.intellij.openapi.util.Pair;
import com.intellij.util.net.HTTPMethod;
import org.junit.Test;

import javax.net.ssl.HttpsURLConnection;
import java.net.URL;

import static org.junit.Assert.*;

public class HttpPutTest {

    @Test
    public void newObject() {
        try {
            new HttpPut(null, new Pair<>("some","pair"));
        } catch (Exception error) {
            fail("call to new HttpPut(null, new Pair<>(\"some\",\"pair\")) generated exception "+ error.toString());
        }

        try {
            new HttpPut("some", null);
        } catch (Exception error) {
            fail("call to new HttpPut(\"some\", null) generated exception "+ error.toString());
        }
    }

    @Test
    public void addParameters() {
        HttpPut testedObject = new HttpPut("some", new Pair<>("some","pair"));
        try {
            testedObject.addParameters(null, "some");
        } catch (Exception error) {
            fail("call to testedObject.addParameters(null, \"some\") generated exception "+ error.toString());
        }

        HttpsURLConnection connection;
        try {
            URL address = new URL("https://www.google.com");
            connection = (HttpsURLConnection) address.openConnection();
            testedObject.addParameters(connection, null);
        } catch (Exception error) {
            fail("call to testedObject.addParameters(connection, null) generated exception "+ error.toString());
        }
    }

    @Test
    public void setMethod() {
        HttpPut testedObject = new HttpPut("some", new Pair<>("some","pair"));
        try {
            testedObject.setMethod(null);
        } catch (Exception error) {
            fail("call to testedObject.setMethod(null) generated exception "+ error.toString());
        }

        HttpsURLConnection connection;
        try {
            URL address = new URL("https://www.google.com");
            connection = (HttpsURLConnection) address.openConnection();
        } catch (Exception ignored) {
            return;
        }

        try {
            testedObject.setMethod(connection);
        } catch (Exception error) {
            fail("call to testedObject.setMethod(connection) generated exception "+ error.toString());
        }

        assertEquals(HTTPMethod.PUT.name(), connection.getRequestMethod());

    }

    @Test
    public void getResponse() {
        HttpPut testedObject = new HttpPut("some", new Pair<>("some","pair"));
        assertTrue(testedObject.getResponse(null,"some").isError());
        assertTrue(testedObject.getResponse("",null).isError());
    }
}