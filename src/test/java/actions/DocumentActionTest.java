/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.util.TextRange;
import org.junit.Test;
import parrent.DocumentHandler;

import static org.junit.Assert.*;

public class DocumentActionTest {

    @Test
    public void update() {
        DummyClass testedObject = new DummyClass();
        try {
            testedObject.update(null);
        } catch (Exception error) {
            fail("Exception generated by call to testedObject.update(null); "+error.toString());
        }
    }

    @Test
    public void actionPerformed() {
        DummyClass testedObject = new DummyClass();
        try {
            testedObject.actionPerformed(null);
        } catch (Exception error) {
            fail("Exception generated by call to testedObject.update(null); "+error.toString());
        }
    }

    class DummyClass extends DocumentAction {

        @Override
        protected TextRange getTextRange(DocumentHandler documentHandler) {
            return null;
        }

        @Override
        protected void action(TextRange textRange, String text) {

        }
    }
}