/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.util.TextRange;
import org.junit.Test;
import parrent.DocumentHandler;

import static org.junit.Assert.*;

public class CloseTaskTest {

    @Test
    public void getTextRange() {
        CloseTask testedObject = new CloseTask();
        assertNull(testedObject.getTextRange(null));
        assertNull(testedObject.getTextRange(new DocumentHandler()));
    }

    @Test
    public void action() {
        CloseTask testedObject = new CloseTask();
        try {
            testedObject.action(null, "some");
        } catch (Exception error){
            fail("call to testedObject.action(null, \"some\"); thrown exception: "+error.toString());
        }

        try {
            testedObject.action(new TextRange(1,1), null);
        } catch (Exception error){
            fail("call to testedObject.action(new TextRange(1,1), \"some\"); thrown exception: "+error.toString());
        }


        try {
            testedObject.action(new TextRange(1,1), "some");
        } catch (Exception error){
            fail("call to testedObject.action(new TextRange(1,1), \"some\"); thrown exception: "+error.toString());
        }
    }
}