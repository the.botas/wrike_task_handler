/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package host;

import com.intellij.openapi.components.BaseComponent;
import com.intellij.openapi.extensions.ExtensionPointName;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Condition;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.messages.MessageBus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.SystemIndependent;
import org.junit.Test;
import org.picocontainer.PicoContainer;
import utils.Result;

import static org.junit.Assert.*;

public class CredentialsManagerTest {
    @Test
    public void getCredentialsManager() {
        Result<CredentialsManager> credentialsManager = CredentialsManager.getCredentialsManager(null);
        assertNotNull(credentialsManager);
        assertTrue(credentialsManager.isOk());
        assertNotNull(credentialsManager.getOk().getIdentity());
        assertNull(credentialsManager.getOk().getIdentity().getToken());

        credentialsManager = CredentialsManager.getCredentialsManager(invalidProject);
        assertNotNull(credentialsManager);
        assertTrue(credentialsManager.isOk());
        assertNotNull(credentialsManager.getOk().getIdentity());
        assertNull(credentialsManager.getOk().getIdentity().getToken());
    }

    @Test
    public void isAuthorized() {
        Result<CredentialsManager> testedObject = CredentialsManager.getCredentialsManager(invalidProject);
        assertFalse(testedObject.getOk().isAuthorized());
    }

    @Test
    public void setToken() {
        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(invalidProject);
        CredentialsManager testedObject = result.getOk();
        assertTrue(testedObject.setToken("invalid"));
        assertFalse(testedObject.setToken(""));
        assertFalse(testedObject.setToken(null));
    }

    @Test
    public void resetToken() {
        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(invalidProject);
        CredentialsManager testedObject = result.getOk();
        Identity identity = testedObject.getIdentity();
        String tokenValue = "some";
        testedObject.setToken(tokenValue);
        assertEquals(tokenValue, identity.getToken());
        testedObject.resetToken();
        assertNull(identity.getToken());
    }

    @Test
    public void setUser() {
        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(invalidProject);
        CredentialsManager testedObject = result.getOk();
        assertFalse(testedObject.setUser(null));
        assertFalse(testedObject.setUser(""));
        assertFalse(testedObject.setUser(" \t\n"));
        assertTrue(testedObject.setUser("123"));
    }

    @Test
    public void setProject() {
        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(invalidProject);
        CredentialsManager testedObject = result.getOk();
        assertFalse(testedObject.setProject(null));
        assertFalse(testedObject.setProject(""));
        assertFalse(testedObject.setProject(" \t\n"));
        assertTrue(testedObject.setProject("123"));
    }

    @Test
    public void saveCredentials() {
        Result<CredentialsManager> testedObject = CredentialsManager.getCredentialsManager(invalidProject);
        assertFalse(testedObject.getOk().saveCredentials().isEmpty());
    }

    @Test
    public void loadLastSavedCredentials() {
        assertFalse(CredentialsManager.getCredentialsManager(invalidProject).getOk().loadLastSavedCredentials().isEmpty());
    }

    private Project invalidProject = new Project() {
        @NotNull
        @Override
        public String getName() {
            return "some";
        }

        @Override
        public VirtualFile getBaseDir() {
            return null;
        }

        @Nullable
        @Override
        public @SystemIndependent String getBasePath() {
            return null;
        }

        @Nullable
        @Override
        public VirtualFile getProjectFile() {
            return null;
        }

        @Nullable
        @Override
        public @SystemIndependent String getProjectFilePath() {
            return null;
        }

        @Nullable
        @Override
        public VirtualFile getWorkspaceFile() {
            return null;
        }

        @NotNull
        @Override
        public String getLocationHash() {
            return null;
        }

        @Override
        public void save() {

        }

        @Override
        public boolean isOpen() {
            return false;
        }

        @Override
        public boolean isInitialized() {
            return false;
        }

        @Override
        public boolean isDefault() {
            return false;
        }

        @Override
        public BaseComponent getComponent(@NotNull String name) {
            return null;
        }

        @Override
        public <T> T getComponent(@NotNull Class<T> interfaceClass) {
            return null;
        }

        @Override
        public <T> T getComponent(@NotNull Class<T> interfaceClass, T defaultImplementationIfAbsent) {
            return null;
        }

        @Override
        public boolean hasComponent(@NotNull Class interfaceClass) {
            return false;
        }

        @NotNull
        @Override
        public <T> T[] getComponents(@NotNull Class<T> baseClass) {
            return null;
        }

        @NotNull
        @Override
        public PicoContainer getPicoContainer() {
            return null;
        }

        @NotNull
        @Override
        public MessageBus getMessageBus() {
            return null;
        }

        @Override
        public boolean isDisposed() {
            return false;
        }

        @NotNull
        @Override
        public <T> T[] getExtensions(@NotNull ExtensionPointName<T> extensionPointName) {
            return null;
        }

        @NotNull
        @Override
        public Condition<?> getDisposed() {
            return null;
        }

        @Override
        public void dispose() {

        }

        @Nullable
        @Override
        public <T> T getUserData(@NotNull Key<T> key) {
            return null;
        }

        @Override
        public <T> void putUserData(@NotNull Key<T> key, @Nullable T value) {

        }
    };

}