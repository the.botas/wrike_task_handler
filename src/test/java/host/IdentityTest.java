/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package host;

import org.junit.Test;

import static org.junit.Assert.*;

public class IdentityTest {

    @Test
    public void setToken() {
        Identity testedObject = new Identity();
        String tokenValue = "some";
        assertTrue(testedObject.setToken(tokenValue));
        assertEquals(tokenValue, testedObject.getToken());
        assertFalse(testedObject.setToken(""));
        assertFalse(testedObject.setToken(null));
    }

    @Test
    public void resetToken() {
        Identity identity = new Identity();
        String tokenValue = "some";
        identity.setToken(tokenValue);
        assertEquals(tokenValue, identity.getToken());
        identity.resetToken();
        assertNull(identity.getToken());
    }

    @Test
    public void setUser() {
        Identity testedObject = new Identity();

        assertFalse(testedObject.setUser(null));
        assertFalse(testedObject.setUser(""));
        assertFalse(testedObject.setUser(" \t\n"));

        String userID = "SOMEID";
        assertTrue(testedObject.setUser(userID));
        assertEquals(userID, testedObject.getUser());
    }

    @Test
    public void setProject() {
        Identity testedObject = new Identity();

        assertFalse(testedObject.setProject(null));
        assertFalse(testedObject.setProject(""));
        assertFalse(testedObject.setProject(" \t\n"));

        String projectID = "SOMEID";
        assertTrue(testedObject.setProject(projectID));
        assertEquals(projectID, testedObject.getProject());
    }

    @Test
    public void asJsonString() {
        Identity testedObject = new Identity();

        assertNull(testedObject.asJsonString());

        String userID = "USER";
        assertTrue(testedObject.setUser(userID));
        assertNull(testedObject.asJsonString());

        testedObject = new Identity();

        String projectID = "PROJECT";
        assertTrue(testedObject.setProject(projectID));
        assertNull(testedObject.asJsonString());

        String expectedResult = "{"+ Identity.JsonField.user+":"+userID+","
                + Identity.JsonField.project+":"+projectID+"}";
        assertTrue(testedObject.setUser(userID));
        assertTrue(testedObject.setProject(projectID));
        assertEquals(expectedResult,testedObject.asJsonString());
    }

    @Test
    public void fromJsonString() {
        Identity testedObject = new Identity();

        assertNotNull(testedObject.fromJsonString(null));
        assertNotNull(testedObject.fromJsonString(""));
        assertNotNull(testedObject.fromJsonString(" \t\n"));
        assertNotNull(testedObject.fromJsonString("some"));

        assertNull(testedObject.fromJsonString("{}"));
        assertEquals("",testedObject.getUser());
        assertEquals("",testedObject.getProject());

        String userID = "USER";
        assertNull(testedObject.fromJsonString("{"+ Identity.JsonField.user+":"+userID+"}"));
        assertEquals(userID, testedObject.getUser());
        assertEquals("",testedObject.getProject());

        String projectID = "PROJECT";
        assertNull(testedObject.fromJsonString("{"+ Identity.JsonField.project+":"+projectID+"}"));
        assertEquals(userID, testedObject.getUser());
        assertEquals(projectID,testedObject.getProject());
    }

}