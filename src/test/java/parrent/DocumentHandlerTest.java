/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package parrent;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.editor.event.*;
import com.intellij.openapi.editor.markup.MarkupModel;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.util.TextRange;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.List;

import static org.junit.Assert.*;

public class DocumentHandlerTest {
    @Test
    public void getSelectionRange() {
        DocumentHandler testedObject = new DocumentHandler();
        assertNull(testedObject.getSelectionRange(null));
        assertNull(testedObject.getSelectionRange(new DummyEditor(0,0)));
        assertNull(testedObject.getSelectionRange(new DummyEditor(0,-1)));
        assertNull(testedObject.getSelectionRange(new DummyEditor(10,10)));
        assertNull(testedObject.getSelectionRange(new DummyEditor(10,9)));
        assertEquals(new TextRange(5,10), testedObject.getSelectionRange(new DummyEditor(5,10)));
    }

    @Test
    public void getCurrentLineRange() {
        DocumentHandler testedObject = new DocumentHandler();
        assertNull(testedObject.getCurrentLineRange(null));
        assertNull(testedObject.getCurrentLineRange(new DummyEditor(0,0)));
        assertNull(testedObject.getCurrentLineRange(new DummyEditor(0,-1)));
        assertNull(testedObject.getCurrentLineRange(new DummyEditor(10,10)));
        assertNull(testedObject.getCurrentLineRange(new DummyEditor(10,9)));
        assertEquals(new TextRange(5,10), testedObject.getCurrentLineRange(new DummyEditor(5,10)));
    }

    class DummyEditor implements Editor {
        int start = 0;
        int end = 0;
        public DummyEditor (int startPosition, int endPosition) {
            start=startPosition;
            end=endPosition;
        }

        @NotNull
        @Override
        public Document getDocument() {
            return null;
        }

        @Override
        public boolean isViewer() {
            return false;
        }

        @NotNull
        @Override
        public JComponent getComponent() {
            return null;
        }

        @NotNull
        @Override
        public JComponent getContentComponent() {
            return null;
        }

        @Override
        public void setBorder(@Nullable Border border) {

        }

        @Override
        public Insets getInsets() {
            return null;
        }

        @NotNull
        @Override
        public SelectionModel getSelectionModel() {
            return new SelectionModel() {
                @Override
                public int getSelectionStart() {
                    return start;
                }

                @Nullable
                @Override
                public VisualPosition getSelectionStartPosition() {
                    return null;
                }

                @Override
                public int getSelectionEnd() {
                    return end;
                }

                @Nullable
                @Override
                public VisualPosition getSelectionEndPosition() {
                    return null;
                }

                @Nullable
                @Override
                public String getSelectedText() {
                    return null;
                }

                @Nullable
                @Override
                public String getSelectedText(boolean allCarets) {
                    return null;
                }

                @Override
                public int getLeadSelectionOffset() {
                    return 0;
                }

                @Nullable
                @Override
                public VisualPosition getLeadSelectionPosition() {
                    return null;
                }

                @Override
                public boolean hasSelection() {
                    return false;
                }

                @Override
                public boolean hasSelection(boolean anyCaret) {
                    return false;
                }

                @Override
                public void setSelection(int startOffset, int endOffset) {

                }

                @Override
                public void setSelection(int startOffset, @Nullable VisualPosition endPosition, int endOffset) {

                }

                @Override
                public void setSelection(@Nullable VisualPosition startPosition, int startOffset, @Nullable VisualPosition endPosition, int endOffset) {

                }

                @Override
                public void removeSelection() {

                }

                @Override
                public void removeSelection(boolean allCarets) {

                }

                @Override
                public void addSelectionListener(@NotNull SelectionListener listener) {

                }

                @Override
                public void removeSelectionListener(@NotNull SelectionListener listener) {

                }

                @Override
                public void selectLineAtCaret() {

                }

                @Override
                public void selectWordAtCaret(boolean honorCamelWordsSettings) {

                }

                @Override
                public void copySelectionToClipboard() {

                }

                @Override
                public void setBlockSelection(@NotNull LogicalPosition blockStart, @NotNull LogicalPosition blockEnd) {

                }

                @NotNull
                @Override
                public int[] getBlockSelectionStarts() {
                    return new int[0];
                }

                @NotNull
                @Override
                public int[] getBlockSelectionEnds() {
                    return new int[0];
                }

                @Override
                public TextAttributes getTextAttributes() {
                    return null;
                }
            };
        }

        @NotNull
        @Override
        public MarkupModel getMarkupModel() {
            return null;
        }

        @NotNull
        @Override
        public FoldingModel getFoldingModel() {
            return null;
        }

        @NotNull
        @Override
        public ScrollingModel getScrollingModel() {
            return null;
        }

        @NotNull
        @Override
        public CaretModel getCaretModel() {
            return new CaretModel() {
                @Override
                public void moveCaretRelatively(int columnShift, int lineShift, boolean withSelection, boolean blockSelection, boolean scrollToCaret) {

                }

                @Override
                public void moveToLogicalPosition(@NotNull LogicalPosition pos) {

                }

                @Override
                public void moveToVisualPosition(@NotNull VisualPosition pos) {

                }

                @Override
                public void moveToOffset(int offset) {

                }

                @Override
                public void moveToOffset(int offset, boolean locateBeforeSoftWrap) {

                }

                @Override
                public boolean isUpToDate() {
                    return false;
                }

                @NotNull
                @Override
                public LogicalPosition getLogicalPosition() {
                    return null;
                }

                @NotNull
                @Override
                public VisualPosition getVisualPosition() {
                    return null;
                }

                @Override
                public int getOffset() {
                    return 0;
                }

                @Override
                public void addCaretListener(@NotNull CaretListener listener) {

                }

                @Override
                public void removeCaretListener(@NotNull CaretListener listener) {

                }

                @Override
                public int getVisualLineStart() {
                    return start;
                }

                @Override
                public int getVisualLineEnd() {
                    return end;
                }

                @Override
                public TextAttributes getTextAttributes() {
                    return null;
                }

                @Override
                public boolean supportsMultipleCarets() {
                    return false;
                }

                @NotNull
                @Override
                public Caret getCurrentCaret() {
                    return null;
                }

                @NotNull
                @Override
                public Caret getPrimaryCaret() {
                    return null;
                }

                @Override
                public int getCaretCount() {
                    return 0;
                }

                @NotNull
                @Override
                public List<Caret> getAllCarets() {
                    return null;
                }

                @Nullable
                @Override
                public Caret getCaretAt(@NotNull VisualPosition pos) {
                    return null;
                }

                @Nullable
                @Override
                public Caret addCaret(@NotNull VisualPosition pos) {
                    return null;
                }

                @Nullable
                @Override
                public Caret addCaret(@NotNull VisualPosition pos, boolean makePrimary) {
                    return null;
                }

                @Override
                public boolean removeCaret(@NotNull Caret caret) {
                    return false;
                }

                @Override
                public void removeSecondaryCarets() {

                }

                @Override
                public void setCaretsAndSelections(@NotNull List<CaretState> caretStates) {

                }

                @Override
                public void setCaretsAndSelections(@NotNull List<CaretState> caretStates, boolean updateSystemSelection) {

                }

                @NotNull
                @Override
                public List<CaretState> getCaretsAndSelections() {
                    return null;
                }

                @Override
                public void runForEachCaret(@NotNull CaretAction action) {

                }

                @Override
                public void runForEachCaret(@NotNull CaretAction action, boolean reverseOrder) {

                }

                @Override
                public void addCaretActionListener(@NotNull CaretActionListener listener, @NotNull Disposable disposable) {

                }

                @Override
                public void runBatchCaretOperation(@NotNull Runnable runnable) {

                }
            };
        }

        @NotNull
        @Override
        public SoftWrapModel getSoftWrapModel() {
            return null;
        }

        @NotNull
        @Override
        public EditorSettings getSettings() {
            return null;
        }

        @NotNull
        @Override
        public EditorColorsScheme getColorsScheme() {
            return null;
        }

        @Override
        public int getLineHeight() {
            return 0;
        }

        @NotNull
        @Override
        public Point logicalPositionToXY(@NotNull LogicalPosition pos) {
            return null;
        }

        @Override
        public int logicalPositionToOffset(@NotNull LogicalPosition pos) {
            return 0;
        }

        @NotNull
        @Override
        public VisualPosition logicalToVisualPosition(@NotNull LogicalPosition logicalPos) {
            return null;
        }

        @NotNull
        @Override
        public Point visualPositionToXY(@NotNull VisualPosition visible) {
            return null;
        }

        @NotNull
        @Override
        public Point2D visualPositionToPoint2D(@NotNull VisualPosition pos) {
            return null;
        }

        @NotNull
        @Override
        public LogicalPosition visualToLogicalPosition(@NotNull VisualPosition visiblePos) {
            return null;
        }

        @NotNull
        @Override
        public LogicalPosition offsetToLogicalPosition(int offset) {
            return null;
        }

        @NotNull
        @Override
        public VisualPosition offsetToVisualPosition(int offset) {
            return null;
        }

        @NotNull
        @Override
        public VisualPosition offsetToVisualPosition(int offset, boolean leanForward, boolean beforeSoftWrap) {
            return null;
        }

        @NotNull
        @Override
        public LogicalPosition xyToLogicalPosition(@NotNull Point p) {
            return null;
        }

        @NotNull
        @Override
        public VisualPosition xyToVisualPosition(@NotNull Point p) {
            return null;
        }

        @NotNull
        @Override
        public VisualPosition xyToVisualPosition(@NotNull Point2D p) {
            return null;
        }

        @Override
        public void addEditorMouseListener(@NotNull EditorMouseListener listener) {

        }

        @Override
        public void removeEditorMouseListener(@NotNull EditorMouseListener listener) {

        }

        @Override
        public void addEditorMouseMotionListener(@NotNull EditorMouseMotionListener listener) {

        }

        @Override
        public void removeEditorMouseMotionListener(@NotNull EditorMouseMotionListener listener) {

        }

        @Override
        public boolean isDisposed() {
            return false;
        }

        @Nullable
        @Override
        public Project getProject() {
            return null;
        }

        @Override
        public boolean isInsertMode() {
            return false;
        }

        @Override
        public boolean isColumnMode() {
            return false;
        }

        @Override
        public boolean isOneLineMode() {
            return false;
        }

        @NotNull
        @Override
        public EditorGutter getGutter() {
            return null;
        }

        @Nullable
        @Override
        public EditorMouseEventArea getMouseEventArea(@NotNull MouseEvent e) {
            return null;
        }

        @Override
        public void setHeaderComponent(@Nullable JComponent header) {

        }

        @Override
        public boolean hasHeaderComponent() {
            return false;
        }

        @Nullable
        @Override
        public JComponent getHeaderComponent() {
            return null;
        }

        @NotNull
        @Override
        public IndentsModel getIndentsModel() {
            return null;
        }

        @NotNull
        @Override
        public InlayModel getInlayModel() {
            return null;
        }

        @NotNull
        @Override
        public EditorKind getEditorKind() {
            return null;
        }

        @Nullable
        @Override
        public <T> T getUserData(@NotNull Key<T> key) {
            return null;
        }

        @Override
        public <T> void putUserData(@NotNull Key<T> key, @Nullable T value) {

        }
    }
}