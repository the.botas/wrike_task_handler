/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package parrent;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserMessageHandlerTest {

    @Test
    public void showEditableChooseDialog() {
        UserMessageHandler testedObject = new UserMessageHandler();
        try {
            testedObject.showEditableChooseDialog(null, "some", new String[]{"some"}, "some");
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showEditableChooseDialog(null, \"some\", new String[]{\"some\"}, \"some\") thrown unexpected exception "+ error.toString());
        }

        try {
            testedObject.showEditableChooseDialog("some", null, new String[]{"some"}, "some");
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showEditableChooseDialog(\"some\", null, new String[]{\"some\"}, \"some\") thrown unexpected exception "+ error.toString());
        }

        assertNull(testedObject.showEditableChooseDialog("some", "some", null, "some") );

        try {
            testedObject.showEditableChooseDialog("some", "some", new String[]{"some"}, null);
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showEditableChooseDialog(\"some\", \"some\", new String[]{\"some\"}, null) thrown unexpected exception "+ error.toString());
        }
    }

    @Test
    public void showInputDialog() {
        UserMessageHandler testedObject = new UserMessageHandler();
        try {
            testedObject.showInputDialog(null, "some");
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showInputDialog(null, \"some\") thrown unexpected exception "+ error.toString());
        }

        try {
            testedObject.showInputDialog("some",null);
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showInputDialog(\"some\", null, \"some\") thrown unexpected exception "+ error.toString());
        }

        try {
            testedObject.showInputDialog(null, "some", "some");
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showInputDialog(null, \"some\", \"some\") thrown unexpected exception "+ error.toString());
        }

        try {
            testedObject.showInputDialog("some",null, "some");
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showInputDialog(\"some\", null, \"some\") thrown unexpected exception "+ error.toString());
        }

        try {
            testedObject.showInputDialog("some","some", null);
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showInputDialog(\"some\", \"some\", null) thrown unexpected exception "+ error.toString());
        }
    }

    @Test
    public void showErrorDialog() {
        UserMessageHandler testedObject = new UserMessageHandler();
        try {
            testedObject.showErrorDialog(null, "some");
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showErrorDialog(null, \"some\") thrown unexpected exception "+ error.toString());
        }

        try {
            testedObject.showErrorDialog("some",null);
        } catch (IllegalStateException ignore) {
        } catch (Exception error) {
            fail("call to testedObject.showErrorDialog(\"some\", null, \"some\") thrown unexpected exception "+ error.toString());
        }
    }
}