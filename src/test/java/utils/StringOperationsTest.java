/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package utils;

import com.intellij.openapi.util.Pair;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringOperationsTest {
    @Test
    public void removeCommentMarkerFromLineStart() {
        String line1 = " some text";
        String result = StringOperations.removeCommentMarkerFromLineStart(StringOperations.commentMarker+line1);
        assertEquals(line1,result);

        String line2 = "New Line text";
        String testText = StringOperations.commentMarker+line1+
                StringOperations.newLineDelimiter+StringOperations.commentMarker+line2;
        result = StringOperations.removeCommentMarkerFromLineStart(testText);
        String expectedResult = line1+StringOperations.newLineDelimiter+line2;
        assertEquals(expectedResult,result);

        String testMultipleCommentTags = StringOperations.commentMarker+StringOperations.commentMarker+StringOperations.commentMarker+line1;
        result = StringOperations.removeCommentMarkerFromLineStart(testMultipleCommentTags);
        assertEquals(line1,result);

        String empty = "";
        result = StringOperations.removeCommentMarkerFromLineStart(empty);
        assertEquals(empty,result);

        result = StringOperations.removeCommentMarkerFromLineStart(null);
        assertNull(result);
    }

    @Test
    public void keepCommentMarkerWhenNotAtLineStart() {
        String line1 = " some text"+StringOperations.commentMarker;
        String result = StringOperations.removeCommentMarkerFromLineStart(line1);
        assertEquals(line1,result);

        String line2 = "New Line"+StringOperations.commentMarker+"text";
        String line3 = "line with comment marker at the end"+StringOperations.commentMarker;
        String line4 = "line with new line char at the end"+StringOperations.newLineDelimiter;
        String testText = line1+
                StringOperations.newLineDelimiter+line2+
                StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+line3+
                StringOperations.newLineDelimiter+line4;
        result = StringOperations.removeCommentMarkerFromLineStart(testText);
        assertEquals(testText,result);
    }

    @Test
    public void splitToListByDelimiter() {
        StringBuilder text = new StringBuilder();
        assertEquals(1, StringOperations.splitToListByDelimiter(text.toString(),StringOperations.newLineDelimiter).size());

        text.append(" some text");
        assertEquals(1, StringOperations.splitToListByDelimiter(text.toString(),StringOperations.newLineDelimiter).size());

        text.append(StringOperations.newLineDelimiter).append(StringOperations.newLineDelimiter);
        assertEquals(3, StringOperations.splitToListByDelimiter(text.toString(),StringOperations.newLineDelimiter).size());

        text.append(StringOperations.newLineDelimiter).append(StringOperations.newLineDelimiter);
        assertEquals(5, StringOperations.splitToListByDelimiter(text.toString(),StringOperations.newLineDelimiter).size());

        text.insert(0, StringOperations.newLineDelimiter);
        assertEquals(6, StringOperations.splitToListByDelimiter(text.toString(),StringOperations.newLineDelimiter).size());

        assertEquals(16, StringOperations.splitToListByDelimiter(text.toString(),"").size());

        assertEquals(1, StringOperations.splitToListByDelimiter("","").size());

        assertNull(StringOperations.splitToListByDelimiter(null,""));

        assertNull(StringOperations.splitToListByDelimiter("",null));
    }

    @Test
    public void addCommentMarker() {
        String text = " somme text";
        assertEquals(StringOperations.commentMarker.concat(" ").concat(text), StringOperations.addCommentMarker(text));

        text = "";
        assertEquals(StringOperations.commentMarker.concat(" ").concat(text), StringOperations.addCommentMarker(text));

        assertNull(StringOperations.addCommentMarker(null));
    }

    @Test
    public void removeEmptyLinesAtBeginningOfText() {
        String singleLineText = " somme text";
        assertEquals(singleLineText, StringOperations.removeEmptyLinesAtBeginningOfText(singleLineText));

        String multiline = StringOperations.newLineDelimiter + singleLineText;
        assertEquals(singleLineText, StringOperations.removeEmptyLinesAtBeginningOfText(multiline));

        String emptyLineWithSpace = ' '+StringOperations.newLineDelimiter + singleLineText;
        assertEquals(singleLineText, StringOperations.removeEmptyLinesAtBeginningOfText(emptyLineWithSpace));

        String emptyLineWithTab = '\t'+StringOperations.newLineDelimiter + singleLineText;
        assertEquals(singleLineText, StringOperations.removeEmptyLinesAtBeginningOfText(emptyLineWithTab));

        String multilineText = singleLineText+
                StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+
                singleLineText;
        assertEquals(multilineText, StringOperations.removeEmptyLinesAtBeginningOfText(multilineText));

        String multilineWithEmptyStartLine = StringOperations.newLineDelimiter+multilineText;
        assertEquals(multilineText, StringOperations.removeEmptyLinesAtBeginningOfText(multilineWithEmptyStartLine));

        String multipleEmptyStartLinea = StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+
                multilineText;
        assertEquals(multilineText, StringOperations.removeEmptyLinesAtBeginningOfText(multipleEmptyStartLinea));

        String multilineWithSpaceStartLine = ' '+StringOperations.newLineDelimiter+multilineText;
        assertEquals(multilineText, StringOperations.removeEmptyLinesAtBeginningOfText(multilineWithSpaceStartLine));

        String multilineWithTabStartLine = '\t'+StringOperations.newLineDelimiter+multilineText;
        assertEquals(multilineText, StringOperations.removeEmptyLinesAtBeginningOfText(multilineWithTabStartLine));

        String empty = "";
        assertEquals(empty,StringOperations.removeEmptyLinesAtBeginningOfText(empty));

        assertNull(StringOperations.removeEmptyLinesAtBeginningOfText(null));
    }

    @Test
    public void getCleanedFirstValidLine() {
        String text = "some text";
        assertEquals(text, StringOperations.getCleanedFirstValidLine(text));

        String testSource = StringOperations.commentMarker+text;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = StringOperations.newLineDelimiter+text;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+text;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = " "+text;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = "\t"+text;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = text+StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = StringOperations.commentMarker+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = StringOperations.newLineDelimiter+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = StringOperations.newLineDelimiter+StringOperations.newLineDelimiter+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = " "+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = "\t"+text+
                StringOperations.newLineDelimiter+text+StringOperations.newLineDelimiter+StringOperations.newLineDelimiter;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        testSource = " \t"+StringOperations.commentMarker+text;
        assertEquals(text, StringOperations.getCleanedFirstValidLine(testSource));

        text = "";
        assertEquals(text, StringOperations.getCleanedFirstValidLine(text));

        assertNull(StringOperations.getCleanedFirstValidLine(null));
    }

    @Test
    public void addIdTagToText() {
        String title = "title line";
        String testTitleLine = " \t"+title;
        String taskID = " "+StringOperations.commentMarker+" SOMEID "+StringOperations.commentMarker;
        String expectedFistLine = testTitleLine+" "+taskID;
        assertEquals(expectedFistLine,StringOperations.addIdTagToText(new Pair<>(title,taskID),testTitleLine));

        StringBuilder taskText = new StringBuilder(testTitleLine);
        taskText.append(StringOperations.newLineDelimiter).append(StringOperations.newLineDelimiter).append(StringOperations.newLineDelimiter).append("contentLine").
                append(StringOperations.newLineDelimiter).append(StringOperations.newLineDelimiter);
        String result = StringOperations.addIdTagToText(new Pair<>(title,taskID),taskText.toString()).
                split(StringOperations.newLineDelimiter,2)[0];
        assertEquals(expectedFistLine, result);

        String newTitle = "New Title";
        expectedFistLine = StringOperations.commentMarker+" "+newTitle+" "+taskID;
        result = StringOperations.addIdTagToText(new Pair<>(newTitle,taskID),taskText.toString()).
                split(StringOperations.newLineDelimiter,2)[0];
        assertEquals(expectedFistLine, result);


        taskText.insert(0, StringOperations.newLineDelimiter);
        result = StringOperations.addIdTagToText(new Pair<>(title,taskID),taskText.toString()).
                split(StringOperations.newLineDelimiter,2)[0];
        expectedFistLine = StringOperations.commentMarker+" "+title+" "+taskID;
        assertEquals(expectedFistLine, result);

        assertNull(StringOperations.addIdTagToText(null,""));
        assertNull(StringOperations.addIdTagToText(new Pair<>(null,""),""));
        assertNull(StringOperations.addIdTagToText(new Pair<>("",null),""));
        assertNull(StringOperations.addIdTagToText(new Pair<>("",""),null));
    }

    @Test
    public void replaceUrlEscapeChars() {
        assertEquals("+", StringOperations.replaceUrlEscapeChars(" ").getOk());
        assertEquals("%24", StringOperations.replaceUrlEscapeChars("$").getOk());
        assertEquals("%26", StringOperations.replaceUrlEscapeChars("&").getOk());
        assertEquals("%60", StringOperations.replaceUrlEscapeChars("`").getOk());
        assertEquals("%3A", StringOperations.replaceUrlEscapeChars(":").getOk());
        assertEquals("%3C", StringOperations.replaceUrlEscapeChars("<").getOk());
        assertEquals("%3E", StringOperations.replaceUrlEscapeChars(">").getOk());
        assertEquals("%5B", StringOperations.replaceUrlEscapeChars("[").getOk());
        assertEquals("%5D", StringOperations.replaceUrlEscapeChars("]").getOk());
        assertEquals("%7B", StringOperations.replaceUrlEscapeChars("{").getOk());
        assertEquals("%7D", StringOperations.replaceUrlEscapeChars("}").getOk());
        assertEquals("%22", StringOperations.replaceUrlEscapeChars("\"").getOk());
        assertEquals("%2B", StringOperations.replaceUrlEscapeChars("+").getOk());
        assertEquals("%23", StringOperations.replaceUrlEscapeChars("#").getOk());
        assertEquals("%25", StringOperations.replaceUrlEscapeChars("%").getOk());
        assertEquals("%40", StringOperations.replaceUrlEscapeChars("@").getOk());
        assertEquals("%2F", StringOperations.replaceUrlEscapeChars("/").getOk());
        assertEquals("%3B", StringOperations.replaceUrlEscapeChars(";").getOk());
        assertEquals("%3D", StringOperations.replaceUrlEscapeChars("=").getOk());
        assertEquals("%3F", StringOperations.replaceUrlEscapeChars("?").getOk());
        assertEquals("%5C", StringOperations.replaceUrlEscapeChars("\\").getOk());
        assertEquals("%5E", StringOperations.replaceUrlEscapeChars("^").getOk());
        assertEquals("%7C", StringOperations.replaceUrlEscapeChars("|").getOk());
        assertEquals("%7E", StringOperations.replaceUrlEscapeChars("~").getOk());
        assertEquals("%27", StringOperations.replaceUrlEscapeChars("'").getOk());
        assertEquals("", StringOperations.replaceUrlEscapeChars("").getOk());
        assertTrue(StringOperations.replaceUrlEscapeChars(null).isError());
    }
}