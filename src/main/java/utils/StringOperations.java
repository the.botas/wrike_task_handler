/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package utils;

import com.intellij.openapi.util.Pair;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringOperations {
    public final static String commentMarker = "//";
    public final static String newLineDelimiter = "\n";
    public final static String htmlLineBreak = "<br>";

    /**
     * @param input the String to be changed
     * @return
     * On success: the input String, where all the start of each line was stripped of comment markers <br>
     * On error: null
     */
    public static String removeCommentMarkerFromLineStart(String input) {

        if (null==input)
            return null;

        List<String> lines = splitToListByDelimiter(input, newLineDelimiter);
        for (int index=0; index<lines.size(); index++) {
            List<String> lineSegments = splitToListByDelimiter(lines.get(index), commentMarker);
            if (1>lineSegments.size())
                continue;

            String newText = lines.get(index);
            for (String segment:lineSegments) {
                if (segment.trim().isEmpty()) {
                    newText = newText.replaceFirst(commentMarker, "");
                } else {
                    break;
                }
            }
            lines.set(index, newText);
        }
        return String.join(newLineDelimiter, lines);
    }

    /**
     * @param text the String to be split
     * @param delimiter the String to use as a delimiter
     * @return
     * On success: a list substring derived from splitting input text. (the delimiter test is not included in the output) <br>
     * On error: null
     */
    public static List<String> splitToListByDelimiter(String text, String delimiter) {
        if (null==text)
            return null;
        if (null==delimiter)
            return null;
        return Arrays.asList(text.split(delimiter, -1));
    }

    /**
     * @param input - String to be changed
     * @return - the input string with comment markers at the start of the line
     */
    public static String addCommentMarker (String input) {
        if (null==input)
            return null;
        return "// ".concat(input);
    }

    /**
     * @param text String to be changed
     * @return
     * On success: the input text without any empty lines on top of the first line with text <br>
     * On error: null
     */
    public static String removeEmptyLinesAtBeginningOfText(String text) {

        if (null==text)
            return null;

        ArrayList<String> textLines = new ArrayList<>(splitToListByDelimiter(text,newLineDelimiter));

        int linesToRemove = 0;
        for (String line : textLines) {
            if (line.trim().isEmpty())
                linesToRemove++;
            else
                break;
        }
        for ( ;linesToRemove>0; linesToRemove--) {
            textLines.remove(0);
        }

        return String.join(newLineDelimiter, textLines);
    }

    /**
     * @param input String to be changed
     * @return
     * On success: the input String stripped of comment markers at the start of the line and of any empty chars (' ', '\t' ...) at the beginning and end of the line <br>
     * On error: null
     */
    public static String getCleanedFirstValidLine (String input) {
        if (null==input)
            return null;

        List<String> lines = StringOperations.splitToListByDelimiter(input,StringOperations.newLineDelimiter);
        for (String fragment: lines) {
            if (null==fragment)
                continue;
            if (fragment.isEmpty())
                continue;
            return StringOperations.removeCommentMarkerFromLineStart(fragment).trim();
        }
        return "";
    }

    /**
     * @param taskData Pair containing task title and task id in that order
     * @param text String to be changed
     * @return
     * On success:the input text modified as follows: <br>
     * - if the first line of the text is equal to the title then the task id is added at the end of the line <br>
     * - if the first line of the text is not equal to the title then a new line with comment markers task title and task id is added on top of the text <br>
     * On error: null
     */
    public static String addIdTagToText(Pair<String,String> taskData, String text) {

        if (null==text)
            return null;
        if (null==taskData)
            return null;
        if (null==taskData.getFirst())
            return null;
        if (null==taskData.getSecond())
            return null;

        String title = taskData.getFirst();
        String id = taskData.getSecond();

        ArrayList<String> lines = new ArrayList<>(StringOperations.splitToListByDelimiter(text,newLineDelimiter));

        String firstLine = lines.get(0);
        String firstLineAsTitle = StringOperations.getCleanedFirstValidLine(firstLine);
        if (firstLineAsTitle.equals(title)) {
            lines.set(0, firstLine.concat(" ").concat(id));
        } else {
            lines.add(0, StringOperations.addCommentMarker(title.concat(" ").concat(id)));
        }

        return String.join(newLineDelimiter,lines);
    }

    /**
     * @param input String to be changed
     * @return
     * On success: a Result object that contains as the valid value the input text converted to the application/x-www-form-urlencoded MIME format.
     * On error: a Result object that contains as the error value the text describing the exception that occurred.
     */
    public  static Result<String> replaceUrlEscapeChars (String input) {
        try {
            return new Result<String>().setOk( URLEncoder.encode( input, StandardCharsets.UTF_8.toString()) );
        } catch (Exception error) {
            return new Result<String>().setError(error.toString());
        }
    }
}
