/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package utils;

import java.util.Optional;

/**
 * Complex result objects where the valid and/or error value can be present or absent
 * @param <T> data type of the valid result. Error type is always String
 */
public class Result<T> {
    private Optional<T> value = Optional.empty();
    private Optional<String> error = Optional.empty();

    /**
     * @param result valid result object to be stored
     * @return An instance of the result object with the valid value stored
     */
    public Result<T> setOk(T result) {
        if (null==result)
            return this;
        value = Optional.of(result);
        return this;
    }

    /**
     * @param message error message to the stored in the result object
     * @return An instance of the result object with the error value stored
     */
    public Result<T> setError(String message) {
        if (null==message)
            return this;
        if (message.trim().isEmpty())
            return this;
        error = Optional.of(message);
        return this;
    }

    /**
     * @return
     * true if the result object has a valid result value <br>
     * false if the valid value is missing
     */
    public boolean isOk() {
        return value.isPresent();
    }

    /**
     * @return
     * true if the result object has an error message <br>
     * false if the error message is missing
     */
    public boolean isError() {
        return error.isPresent();
    }

    /**
     * @return the error message as String
     */
    public String getError() {
        return error.orElse("");
    }

    /**
     * @return the stored valid value
     */
    public T getOk() {
        return value.orElse(null);
    }
}
