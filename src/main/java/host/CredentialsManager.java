/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package host;

import com.intellij.credentialStore.CredentialAttributes;
import com.intellij.credentialStore.CredentialAttributesKt;
import com.intellij.credentialStore.Credentials;
import com.intellij.ide.passwordSafe.PasswordSafe;
import com.intellij.openapi.project.Project;
import utils.Result;

public class CredentialsManager {
    private static Identity identity = new Identity();
    private static Project project = null;

    private static CredentialsManager credentialsManager;
    private CredentialsManager () {
    }

    private static CredentialAttributes createCredentialAttributes() {
        if (null==project)
            return null;
        try {
            return new CredentialAttributes(CredentialAttributesKt.generateServiceName("WrikeTaskHandler_", System.getProperty("os.name") + "_" + System.getProperty("user.name")));
        } catch (Exception ignore) {
            return null;
        }
    }

    /**
     * @param openedProject an instance of the Intellij project currently opened
     * @return a Result object that contains a credentials manager instance in the valid value.
     */
    public static Result<CredentialsManager> getCredentialsManager(Project openedProject) {
        Result<CredentialsManager> result = new Result<>();

        if (null!=openedProject)
            project = openedProject;

        if (null==credentialsManager) {
            credentialsManager = new CredentialsManager();
            credentialsManager.loadLastSavedCredentials();
        }

        return result.setOk(credentialsManager);
    }

    /**
     * Attempts to load the last stored credential values
     * @return
     * On success: Null <br>
     * On error: a string describing the error that occurred
     */
    public String loadLastSavedCredentials() {
        if (null == project)
            return "Invalid project value!";

        CredentialAttributes credentialAttributes = createCredentialAttributes();
        if (null == credentialAttributes)
            return "Could not create new credentials!";

        Credentials credentials;
        try {
            credentials = PasswordSafe.getInstance().get(credentialAttributes);
        } catch (Exception ignore) {
            return "No credentials where previously saved for this project.";
        }

        if (credentials == null) {
            return "No credentials where previously saved for this project.";
        }

        String error = identity.fromJsonString(credentials.getUserName());
        String fixSuggestion = "Please attempt to update credentials.";
        if (null != error)
            return error + " " + fixSuggestion;
        if (!identity.setToken(credentials.getPasswordAsString()))
            return "Key value could not be parsed! " + fixSuggestion;

        return null;
    }

    /**
     * @return
     * true if a token value is stored <br>
     * false if not token value is stored
     */
    public boolean isAuthorized() {
        return (null!=identity.getToken());
    }

    /**
     * @param value the token to be saved
     * @return
     * On success: true <br>
     * On error: false (example if the input parameter is invalid)
     */
    public boolean setToken(String value) {
        return identity.setToken(value);
    }

    /**
     * @return the Identity object containing the identification data
     */
    public Identity getIdentity() {
        return identity;
    }

    /**
     * Deletes the last stored token
     */
    public void resetToken() {
        identity.resetToken();
    }

    /**
     * @param user the ID of the user to be saved
     * @return
     * On success: true <br>
     * On error: false (example if the input parameter is invalid)
     */
    public boolean setUser(String user) {
        return identity.setUser(user);
    }

    /**
     * @param project the ID of the project to be saved
     * @return
     * On success: true <br>
     * On error: false (example if the input parameter is invalid)
     */
    public boolean setProject(String project) {
        return identity.setProject(project);
    }

    /**
     * Saves the last set credentials.
     * @return
     * On success: null <br>
     * On error: a string describing the error that occurred
     */
    public String saveCredentials() {
        CredentialAttributes credentialAttributes = createCredentialAttributes();

        if (null==credentialAttributes)
            return "Could not initiate Credentials Attributes!";

        String identityData = identity.asJsonString();
        if (null==identityData)
            return "Invalid identity data! Please supply valid identity data.";

        try {
            PasswordSafe passwordSafe = PasswordSafe.getInstance();

            if (isAuthorized()) {
                passwordSafe.set(credentialAttributes, null);
            }

            Credentials credentials = new Credentials(identityData, identity.getToken());
            passwordSafe.set(credentialAttributes, credentials);
        } catch (Exception ignore) {
            return "Error updating saved credentials!";
        }

        return null;
    }
}
