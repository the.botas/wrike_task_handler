/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package host;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class Identity {
    private String token = null;
    private String project = "";
    private String user = "";

    protected final class JsonField {
        static final String project = "project";
        static final String user = "name";
    }

    /**
     * @param value - the token to be saved
     * @return
     * On success: true <br>
     * On error: false (example if the input parameter is invalid)
     */
    public boolean setToken (String value) {
        if (null==value) { return false; }
        if (value.trim().isEmpty()) { return false; }
        token = value;
        return true;
    }

    /**
     * Deletes the last stored token
     */
    public void resetToken() {
        token = null;
    }

    /**
     * @param userID the ID of the user to be saved
     * @return
     * On success: true <br>
     * On error: false (example if the input parameter is invalid)
     */
    public boolean setUser (String userID) {
        if (null==userID) { return false; }
        if (userID.trim().isEmpty()) { return false; }
        user = userID;
        return true;
    }

    /**
     * @param projectID the ID of the project to be saved
     * @return
     * On success: true <br>
     * On error: false (example if the input parameter is invalid)
     */
    public boolean setProject (String projectID) {
        if (null==projectID) { return false; }
        if (projectID.trim().isEmpty()) { return false; }
        project = projectID;
        return true;
    }

    /**
     * @return
     * On success: a JSON string that contains the user and project value <br>
     * On error: null
     */
    public String asJsonString() {
        if (project.isEmpty()) return null;
        if (user.isEmpty()) return null;
        return "{"+JsonField.user +":"+ user +","+JsonField.project+":"+project+"}";
    }

    /**
     * @param rawData JSON string containing user and project values
     * @return
     * On success: Null <br>
     * On error: a string describing the error that occurred
     */
    public String fromJsonString (String rawData){

        if (null==rawData) {
            return "Could not interpret null credentials data!";
        }
        if (rawData.trim().isEmpty()) {
            return "Could not interpret empty credentials data!";
        }

        JsonObject sourceData;
        try {
            sourceData = new GsonBuilder().create().fromJson(rawData, JsonObject.class);
        } catch (Exception error) {
            return "Credentials data is not in a supported format!";
        }

        if (null==sourceData){
            return "Credentials data is not in a supported format!";
        }

        if (sourceData.has(JsonField.user)) {
            user = sourceData.get(JsonField.user).getAsString();
        }

        if (sourceData.has(JsonField.project)) {
            project = sourceData.get(JsonField.project).getAsString();
        }

        return null;
    }

    /**
     * @return the project ID of the last saved project
     */
    public String getProject() {
        return project;
    }

    /**
     * @return the user ID of the last saved user
     */
    public String getUser() {
        return user;
    }

    /**
     * @return the current value of the access token
     */
    public String getToken() {
        return token;
    }
}
