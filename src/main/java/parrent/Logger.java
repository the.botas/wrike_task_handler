/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package parrent;

import com.intellij.notification.*;
import com.intellij.openapi.project.Project;

public class Logger {
    private static Project project;
    private static NotificationGroup notification;
    private static Logger logger;

    private Logger() {
        notification = new NotificationGroup("wrike_task_handler", NotificationDisplayType.BALLOON, true);
    }

    /**
     * @param newProject the IntelliJ project currently opened
     * @return a logger instance
     */
    public static Logger getLogger(Project newProject) {
        if (null!=newProject)
            project = newProject;
        return getLogger();
    }

    private static Logger getLogger() {
        if (null==logger)
            logger = new Logger();
        return logger;
    }

    /**
     * @param message message to be shown as error to the user
     * @param title message title
     */
    public void error(String message, String title) {
        if (null==notification) return;
        if (null==message) message="";
        if (null==title) title="Null title";
        if (title.isEmpty()) title="Empty title";
        notify(notification.createNotification(title, message, NotificationType.ERROR, null));
    }

    /**
     * @param message message to be shown as warning to the user
     * @param title message title
     */
    public void warn(String message, String title) {
        if (null==notification) return;
        if (null==message) message="";
        if (null==title) title="Null title";
        if (title.isEmpty()) title="Empty title";
        notify(notification.createNotification(title, message, NotificationType.WARNING, null));
    }

    /**
     * @param message message to be shown as information to the user
     * @param title message title
     */
    public void info(String message, String title) {
        if (null==notification) return;
        if (null==message) message="";
        if (null==title) title="Null title";
        if (title.isEmpty()) title="Empty title";
        notify(notification.createNotification(title, message, NotificationType.INFORMATION, null));
    }

    private void notify(Notification notification) {
        try {
            if (null != project)
                Notifications.Bus.notify(notification, project);
            else
                Notifications.Bus.notify(notification);
        } catch (Exception ignored) {}
    }
}
