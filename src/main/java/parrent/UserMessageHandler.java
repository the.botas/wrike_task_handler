/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package parrent;

import com.intellij.openapi.ui.InputValidator;
import com.intellij.openapi.ui.Messages;

public class UserMessageHandler {

    /**
     * @param message string describing to the user the expected input content
     * @param title string to be shown as the title of the dialog
     * @param options options from which the user can select
     * @param defaultOption default selected option
     * @return
     * If the user pressed ok: the text of the option selected by the user <br>
     * If the user pressed cancel: null <br>
     * The user will not be able to select OK unless the text in the input box is one of the options
     */
    public String showEditableChooseDialog(String message, String title, String[] options, String defaultOption) {
        if (null==message) message = "";
        if (null==title) title = "";
        if (null==options) return null;
        if (null==defaultOption) defaultOption = "";
        return Messages.showEditableChooseDialog(message, title, null, options, defaultOption, new InputValidator() {
            @Override
            public boolean checkInput(String inputString) {
                if (null==inputString)
                    return false;
                for (String value: options ) {
                    if (value.equals(inputString))
                        return true;
                }
                return false;
            }

            @Override
            public boolean canClose(String inputString) {
                return true;
            }
        });
    }

    /**
     * @param message string describing to the user the expected input content
     * @param title string to be shown as the title of the dialog
     * @return the text introduced by the user
     */
    public String showInputDialog (String message, String title) {
        if (null==message) message = "";
        if (null==title) title = "";
        return Messages.showInputDialog(message, title, null);
    }

    /**
     * @param message string describing to the user the expected input content
     * @param title string to be shown as the title of the dialog
     * @param prefilledValue initial content of the user input box
     * @return the text introduced by the user
     */
    public String showInputDialog (String message, String title, String prefilledValue) {
        if (null==message) message = "";
        if (null==title) title = "";
        if (null==prefilledValue) prefilledValue = "";
        return Messages.showInputDialog(message, title, null, prefilledValue, null);
    }

    /**
     * @param message string to be shown in the error dialog
     * @param title string to be shown as the title of the dialog
     */
    public void showErrorDialog (String message, String title) {
        if (null==message) message = "";
        if (null==title) title = "";
        Messages.showErrorDialog(message, title);
    }
}
