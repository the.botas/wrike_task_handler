/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package parrent;

import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.editor.Editor;

public class DocumentHandler {

    /**
     * @param editor the Intellij editor where the event occurred
     * @return the Text range containing the selected text when the event occurred
     */
    public TextRange getSelectionRange(Editor editor) {
        if (null==editor) return null;
        final SelectionModel selectionModel = editor.getSelectionModel();
        int start = selectionModel.getSelectionStart();
        int end = selectionModel.getSelectionEnd();
        if (end<=start) return null;
        return new TextRange(start, end);
    }

    /**
     * @param editor the intellij editor where the event occurred
     * @return the Text range containing the full line where the cursor was when the event occurred
     */
    public TextRange getCurrentLineRange(Editor editor) {
        if (null==editor) return null;
        final CaretModel caretModel = editor.getCaretModel();
        int start = caretModel.getVisualLineStart();
        int end = caretModel.getVisualLineEnd();
        if (end<=start) return null;
        return new TextRange(start, end);

    }
}
