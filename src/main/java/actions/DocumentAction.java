/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.util.TextRange;
import database.TaskController;
import parrent.DocumentHandler;
import parrent.Logger;
import parrent.UserMessageHandler;
import host.CredentialsManager;
import utils.Result;
import utils.StringOperations;

public abstract class DocumentAction extends EditorAction {
    private TextRange textRange;
    TaskController taskController;

    /**
     * Sets the visibility of the action to true or false. Action should not be visible if a connection was not established and no text was selected
     * @param trigger the event trigger
     */
    @Override
    public void update(AnActionEvent trigger) {
        if (null==trigger) return;

        super.update(trigger);
        if (!isVisible) return;

        if (null==project) {
            trigger.getPresentation().setVisible(false);
            return;
        }

        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(project);
        credentialsManager = result.getOk();

        if(result.isError()) {
            trigger.getPresentation().setVisible(false);
            if (null != log)
                log.error(result.getError(), "Security Manager Error");
        }

        if (!credentialsManager.isAuthorized()) {
            trigger.getPresentation().setVisible(false);
            return;
        }

        if (null==editor) {
            trigger.getPresentation().setVisible(false);
            return;
        }

        document = editor.getDocument();

        textRange = getTextRange(new DocumentHandler());
        if (null==textRange) {
            trigger.getPresentation().setVisible(false);
        }
    }

    /**
     * @param trigger calls the action method defined child classes
     */
    @Override
    public void actionPerformed(AnActionEvent trigger) {
        if (null==trigger) return;
        log = Logger.getLogger(project);
        userMessageHandler = new UserMessageHandler();
        taskController = new TaskController(credentialsManager.getIdentity());
        if (null==textRange) return;

        String text = document.getText(textRange);

        String textBuffer = text;
        if (textBuffer
                .replace(" ","")
                .replace(StringOperations.newLineDelimiter, "")
                .replace("\t", "")
                .trim()
                .isEmpty()) {

            String errorTitle = "Invalid Input!";
            String error = "The selected text has no valid characters.";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);

            return;
        }

        action(textRange, text);
    }

    protected abstract TextRange getTextRange(DocumentHandler documentHandler);

    protected abstract void action(TextRange textRange, String text);
}
