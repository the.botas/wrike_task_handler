/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.editor.Document;
import parrent.Logger;
import parrent.UserMessageHandler;
import host.CredentialsManager;

public abstract class EditorAction extends AnAction {
    protected Project project;
    Editor editor;
    Document document;
    UserMessageHandler userMessageHandler;
    CredentialsManager credentialsManager;
    Logger log;
    Boolean isVisible = true;

    /**
     * Sets the visibility of the action to true or false. Action should not be visible a editor and project are not opened.
     * @param trigger the event trigger
     */
    @Override
    public void update(AnActionEvent trigger) {
        if (null==trigger) {
            isVisible = false;
            return;
        }
        project = trigger.getProject();
        editor = trigger.getData(CommonDataKeys.EDITOR);
        isVisible = (project != null && editor != null);
        trigger.getPresentation().setVisible(isVisible);
    }
}
