/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.util.Pair;
import com.intellij.openapi.util.TextRange;
import parrent.DocumentHandler;
import utils.Result;
import utils.StringOperations;

public class CreateTask extends DocumentAction {

    /**
     * @param documentHandler the document that was used to generate the event trigger.
     * @return
     * On success: the line where the cursor was when the event occurred or the selected text <br>
     * On error: null
     */
    protected TextRange getTextRange(DocumentHandler documentHandler) {
        if (null==documentHandler) return null;
        if (null==editor) return null;

        TextRange newRange;
        if (editor.getSelectionModel().hasSelection()) {
            newRange =  documentHandler.getSelectionRange(editor);
        } else {
            TextRange rangeOfCurrentLine = documentHandler.getCurrentLineRange(editor);
            if (null==rangeOfCurrentLine) return null;
            newRange = new TextRange(rangeOfCurrentLine.getStartOffset(), rangeOfCurrentLine.getEndOffset()-1);
        }

        if (null==newRange) return null;
        if (1>=newRange.getLength()) return null;

        return newRange;
    }

    /**
     * @param textRange text range used define the limits of the original text
     * @param description selected text to be used for the new task
     */
    protected void action(TextRange textRange, String description) {

        if (null==textRange) {
            String errorTitle = "Action Input Error";
            String error = "Text range can not be null!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        if (null==description) {
            String errorTitle = "Action Input Error";
            String error = "Description can not be null!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        String cleanDescription = StringOperations.removeEmptyLinesAtBeginningOfText(description);

        if (null==cleanDescription) {
            String errorTitle = "Action Input Error";
            String error = "Invalid task description field selected!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        String proposedTitle = StringOperations.getCleanedFirstValidLine(cleanDescription);
        if (null==proposedTitle) {
            proposedTitle = "";
        }

        if (null==userMessageHandler){
            if (null!=log)
                log.error("userMessageHandler was not initialized!", "Initialization error");
            return;
        }

        String title = userMessageHandler.showInputDialog("Input task tile","Task Configuration", proposedTitle);
        if (null==title) {
            if (null!=log)
                log.info("Task creation process canceled by user.", "Info");
            return;
        }

        if (null==taskController) {
            String errorTitle = "Initialization error";
            String error = "taskController was not initialized!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        Result<Pair<String,String>> result = taskController.createTaskFromString(cleanDescription, title);
        if (result.isError()) {
            String errorTitle = "Task Creation Error";
            if (null!=log)
                log.error(result.getError(), errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(result.getError(), errorTitle);
        }

        if (result.isOk()) {

            String errorTitle = "Task Creation Error";
            if (null==project) {
                String errorMessage = "Intellij project not initiated!";
                if (null!=log)
                    log.error(errorMessage, errorTitle);
                if (null!=userMessageHandler)
                    userMessageHandler.showErrorDialog(errorMessage, errorTitle);
                return;
            }

            if (null==document) {
                String errorMessage = "Intellij document is null!";
                if (null!=log)
                    log.error(errorMessage, errorTitle);
                if (null!=userMessageHandler)
                    userMessageHandler.showErrorDialog(errorMessage, errorTitle);
                return;
            }

            WriteCommandAction.runWriteCommandAction(project, () ->
                    document.replaceString(textRange.getStartOffset(), textRange.getEndOffset(), StringOperations.addIdTagToText(result.getOk(), cleanDescription))
            );
        }
    }

}
