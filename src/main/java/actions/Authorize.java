/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import database.UserController;
import org.jetbrains.annotations.Nullable;
import utils.Result;

public class Authorize extends ConnectionSetupAction {

    /**
     * Requests from the user a token value and tests if the token is valid
     * @return
     * true if the new token is valid <br>
     * false if the token is not valid or an error occurred
     */
    protected Boolean updateToken() {
        String newToken = getWrikeTokenFromUser();
        if (null == newToken) { return false; }

        if (null == credentialsManager) {
            String errorTitle = "Initialization error";
            String errorMessage = "credentialsManager was not initiated!";
            if (null!=log)
                log.error(errorMessage, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(errorMessage,errorTitle);
            return false;
        }

        if (!credentialsManager.setToken(newToken)) {
            String errorTitle = "Input Error";
            String errorMessage = "Invalid security token!";
            if (null!=log)
                log.error(errorMessage, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(errorMessage,errorTitle);
            return false;
        }

        UserController userController = new UserController(credentialsManager.getIdentity());
        Result<String> me = userController.getMyDatabaseId();
        if (me.isError()) {
            String errorTitle = "Credentials Error";
            if (null!=log)
                log.error(me.getError(), errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(me.getError(),errorTitle);
            return false;
        }

        if(!credentialsManager.setUser(me.getOk())) {
            String errorTitle = "Credentials Error";
            String errorMessage = "Invalid user id!";
            if (null!=log)
                log.error(errorMessage, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(errorMessage,errorTitle);
            return false;
        }

        return true;
    }

    /**
     * @return
     * On success: the token value inserted by the user in the input box <br>
     * On error: null
     */
    @Nullable
    private String getWrikeTokenFromUser() {

        if (null==userMessageHandler){
            if (null!=log)
                log.error("userMessageHandler was not initialized!", "Initialization error");
            return null;
        }

        String newToken = userMessageHandler.showInputDialog("Please input the Wrike permanent application token","Configure Connection");
        if (null==newToken) {
            if (null!=log)
                log.info("Authorization process canceled by user.", "Info");
            return null;
        }

        String errorTitle = "Authorization Error";
        if (newToken.isEmpty()) {
            if (null!=log)
                log.error("Access token can not be empty!", errorTitle);
            return null;
        }

        return newToken;
    }

    /**
     * Writes in the event log a message indicating that the operation ended successfully
     */
    protected void logSuccessMessage() {
        if (null!=log)
            log.info("You are now authorized to edit tasks.","Connection Setup");
    }

}
