/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import database.ProjectController;
import parrent.Logger;
import parrent.UserMessageHandler;
import host.CredentialsManager;
import utils.Result;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class ConnectionSetupAction extends EditorAction {

    /**
     * Sets the visibility of the action to true or false.
     * @param trigger the event trigger
     */
    @Override
    public void update(AnActionEvent trigger) {
        if (null==trigger) return;
        super.update(trigger);
        if (!isVisible) return;
        alterVisibility(trigger);
    }

    /**
     * This method calls all the methods required to execute the authorization process
     * @param trigger event that triggered the action
     */
    @Override
    public void actionPerformed(AnActionEvent trigger) {
        if (null==trigger) return;
        log = Logger.getLogger(project);
        userMessageHandler = new UserMessageHandler();
        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(project);
        credentialsManager = result.getOk();

        String errorTitle = "Security Manager Error";
        if(result.isError()) {
            if (null!=log)
                log.error(result.getError(), errorTitle);
            return;
        }

        if (!updateToken()) {
            if (null!=credentialsManager.loadLastSavedCredentials())
                credentialsManager.resetToken();
            return;
        }

        if (!updateProject()) {
            if (null!=credentialsManager.loadLastSavedCredentials())
                credentialsManager.resetToken();
            return;
        }

        String error = credentialsManager.saveCredentials();
        if (null!=error) {
            if (null!=credentialsManager.loadLastSavedCredentials())
                credentialsManager.resetToken();
            log.error(error, errorTitle);
            userMessageHandler.showErrorDialog(error,errorTitle);
            return;
        }

        logSuccessMessage();
    }

    /**
     * @param trigger this method is empty
     */
    protected void alterVisibility(AnActionEvent trigger) {}

    /**
     * @return always true
     */
    protected Boolean updateToken() { return true; }

    /**
     * @return
     * true if the stored project ID was updated <br>
     * false if saving project ID failed
     */
    private boolean updateProject() {
        if (null==credentialsManager) {
            String errorTitle = "Initialization Error";
            String error = "credentialsManager was not initialized!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return false;
        }

        ProjectController projectController = new ProjectController(credentialsManager.getIdentity());
        Result<HashMap<String, String>> databaseProjects = projectController.getProjects();

        if (databaseProjects.isError()) {
            String errorTitle = "Database Connection Error";
            if (null!=log)
                log.error(databaseProjects.getError(), errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(databaseProjects.getError(), errorTitle);
            return false;
        }

        if ( databaseProjects.getOk().size() <= 0) {
            if (null!=log)
                log.warn("No projects found in the database! This will inhibit the ability of creating a new task.", "Database warning");
        }

        String projectId = getProjectIdSelectedByUser(databaseProjects.getOk());
        if (null==projectId) {
            if (null!=log)
                log.info("Authorization process canceled by user.", "Info");
            return false;
        }

        if(!credentialsManager.setProject(projectId)) {
            String errorTitle = "User Input Error";
            String errorMessage = "Invalid selected project!";
            if (null!=log)
                log.error(errorMessage, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(errorMessage,errorTitle);
            return false;
        }
        return true;
    }

    /**
     * @param projects a hash map containing pairs of project ID and project name
     * @return
     * On success: a string with the selected project id <br>
     * On error: null
     */
    private String getProjectIdSelectedByUser(HashMap<String,String> projects) {
        if (null==projects)
            return null;

        ArrayList<String> projectNames = new ArrayList<>();
        ArrayList<String> projectIds = new ArrayList<>();
        String indexDelimiter = ": ";
        int indexCount = 0;
        for (HashMap.Entry<String, String> projectData : projects.entrySet()) {
            String nameToAdd = indexCount + indexDelimiter + projectData.getValue();
            projectNames.add(nameToAdd);
            projectIds.add(projectData.getKey());
            indexCount++;
        }

        if (null==credentialsManager) {
            String errorTitle = "Initialization Error";
            String error = "credentialsManager was not initialized!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return null;
        }

        if (null==userMessageHandler) {
            if (null!=log)
                log.error("userMessageHandler was not initialized!", "Initialization error");
            return null;
        }

        String selectedProjectName = userMessageHandler.showEditableChooseDialog(
                "Choose the project you want to use",
                "Project Selector",
                projectNames.toArray(new String[0]),
                projectNames.get(0));

        if (null==selectedProjectName)
            return null;

        String[] selectionData = selectedProjectName.split(indexDelimiter, 2);
        int index;
        try {
            index = Integer.parseInt(selectionData[0].trim());
        } catch (Exception ignore) {
            return null;
        }

        return projectIds.get(index);
    }

    protected abstract void logSuccessMessage();
}
