/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import database.UserController;
import parrent.Logger;
import parrent.UserMessageHandler;
import host.CredentialsManager;
import utils.Result;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectResponsible extends EditorAction {

    /**
     * Sets the visibility of the action to true or false. Action should not be visible if a connection was not established.
     * @param trigger the event trigger
     */
    @Override
    public void update(AnActionEvent trigger) {
        if (null==trigger) return;

        super.update(trigger);
        if (!isVisible) return;

        if (null==project){
            trigger.getPresentation().setVisible(false);
            return;
        }

        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(project);
        credentialsManager = result.getOk();

        if(result.isError())
            if (null!=log)
                log.error(result.getError(), "Security Manager Error");

        if (!credentialsManager.isAuthorized())
            trigger.getPresentation().setVisible(false);
    }

    /**
     * Handles the select responsible request
     * @param trigger the event trigger
     */
    @Override
    public void actionPerformed(AnActionEvent trigger) {
        if (null==trigger) return;

        log = Logger.getLogger(project);
        userMessageHandler = new UserMessageHandler();
        UserController userController = new UserController(credentialsManager.getIdentity());

        Result<HashMap<String, String>> databaseUsers = userController.getUsers();

        if (databaseUsers.isError()) {
            String errorTitle = "Database Connection Error";
            log.error(databaseUsers.getError(), errorTitle);
            userMessageHandler.showErrorDialog(databaseUsers.getError(), errorTitle);
            return;
        }

        if ( databaseUsers.getOk().size() <= 0) {
            log.warn("No users found in the database!", "Database warning");
        }

        String userId = getUserIdSelectedByUser(databaseUsers.getOk());
        if (null==userId) {
            log.info("User selection process canceled by user.", "Info");
            return;
        }

        if(!credentialsManager.setUser(userId)) {
            String errorTitle = "User Input Error";
            String errorMessage = "Invalid selected user!";
            log.error(errorMessage, errorTitle);
            userMessageHandler.showErrorDialog(errorMessage, errorTitle);
            return;
        }

        String error = credentialsManager.saveCredentials();
        if (null!=error) {
            String errorTitle = "Security Manager Error";
            log.error(error, errorTitle);
            userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        log.info("Selected responsible saved.","Connection Setup");
    }

    private String getUserIdSelectedByUser(HashMap<String,String> users) {
        if (null==users)
            return null;

        ArrayList<String> userNames = new ArrayList<>();
        ArrayList<String> userIds = new ArrayList<>();
        String indexDelimiter = ": ";
        int indexCount = 0;
        for (HashMap.Entry<String, String> userData : users.entrySet()) {
            String nameToAdd = indexCount + indexDelimiter + userData.getValue();
            userNames.add(nameToAdd);
            userIds.add(userData.getKey());
            indexCount++;
        }

        if (null==userMessageHandler) return null;

        String selectedUserName = userMessageHandler.showEditableChooseDialog(
                "The user you select will be set as a responsible for newly created tasks.",
                "Responsible selection",
                userNames.toArray(new String[0]),
                userNames.get(0));

        if (null==selectedUserName)
            return null;

        String[] selectionData = selectedUserName.split(indexDelimiter, 2);
        int index;
        try {
            index = Integer.parseInt(selectionData[0].trim());
        } catch (Exception ignore) {
            return null;
        }

        return userIds.get(index);
    }
}
