/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.util.TextRange;
import parrent.DocumentHandler;

public class CloseTask extends DocumentAction {
    /**
     * @param documentHandler the document that was used to generate the event trigger.
     * @return
     * On success: the line where the cursor was when the event occurred or the selected text <br>
     * On error: null
     */
    protected TextRange getTextRange(DocumentHandler documentHandler) {
        if (null==documentHandler)
            return null;

        if (null==editor)
            return null;

        TextRange newRange;
        if (editor.getSelectionModel().hasSelection()) {
            newRange = documentHandler.getSelectionRange(editor);
        } else {
            newRange = documentHandler.getCurrentLineRange(editor);
        }

        if (null==newRange) return null;
        if (1>=newRange.getLength()) return null;

        return newRange;
    }

    /**
     * @param textRange text range used define the limits of the original text
     * @param text selected text with one wrike task id field
     */
    protected void action(TextRange textRange, String text){
        String errorTitle = "Task Update Error";
        if (null==textRange) {
            String error = "Text range can not be null!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        if (null==text) {
            String error = "Text can not be null!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        if (null==taskController) {
            String error = "Task controller was not initiated!";
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        String error = taskController.closeTask(text);
        if (null!=error) {
            if (null!=log)
                log.error(error, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(error, errorTitle);
            return;
        }

        if (null==project) {
            String errorMessage = "Intellij project not initiated!";
            if (null!=log)
                log.error(errorMessage, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(errorMessage, errorTitle);
            return;
        }

        if (null==document) {
            String errorMessage = "Intellij document is null!";
            if (null!=log)
                log.error(errorMessage, errorTitle);
            if (null!=userMessageHandler)
                userMessageHandler.showErrorDialog(errorMessage, errorTitle);
            return;
        }

        WriteCommandAction.runWriteCommandAction(project, () ->
                document.replaceString(textRange.getStartOffset(), textRange.getEndOffset(), "") );

        if (null!=log)
            log.info("Task was successfully closed. The selected text was removed. " +
                    "Using undo now will bring back the text but will not alter the state of the task in the database",
                    "Successful Operation");
    }
}