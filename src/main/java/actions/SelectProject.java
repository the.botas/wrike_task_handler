/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import host.CredentialsManager;
import utils.Result;

public class SelectProject extends ConnectionSetupAction {

    /**
     * Sets the visibility of the action to true or false. Action should not be visible if a connection was not established.
     * @param trigger the event trigger
     */
    @Override
    protected void alterVisibility(AnActionEvent trigger) {
        if (null==trigger) return;

        if (null==project) {
            trigger.getPresentation().setVisible(false);
            return;
        }

        Result<CredentialsManager> result = CredentialsManager.getCredentialsManager(project);
        credentialsManager = result.getOk();

        if(result.isError())
            if (null!=log)
                log.error(result.getError(), "Security Manager Error");

        if (!credentialsManager.isAuthorized())
            trigger.getPresentation().setVisible(false);
    }

    /**
     * Writes in the event log a message indicating that the operation ended successfully
     */
    protected void logSuccessMessage() {
        if (null!=log)
            log.info("Selected project saved.","Connection Setup");
    }
}
