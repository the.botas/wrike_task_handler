/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.HTTP;

import com.intellij.openapi.util.Pair;
import utils.Result;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public abstract class HttpRequest {

    protected abstract String baseAddress();
    protected abstract Pair<String,String> getAuthorizationProperty();
    protected abstract void addParameters (HttpsURLConnection connection, String parameters) throws Exception;
    protected abstract void setMethod(HttpsURLConnection connection) throws Exception;

    /**
     * @param path database server address
     * @param parameters http request parameters
     * @return
     * On success: a Result object containing in the valid value a pair of the response code and the response content <br>
     * On error: a Result object containing in the valid value a string that describes the error that occurred
     */
    public Result<Pair<Integer,String>> getResponse (String path, String parameters) {
        Result<Pair<Integer,String>> result = new Result<>();

        if (null==path)
            return result.setError("Input path can not be null");
        if (path.trim().isEmpty())
            return result.setError("Input path can not be empty");

        if (null==parameters)
            parameters = "";

        HttpsURLConnection connection = null;
        int responseCode;
        StringBuilder responseData = new StringBuilder();
        try {
            URL address = new URL(baseAddress() + path);
            connection = (HttpsURLConnection) address.openConnection();

            Pair<String, String> authorizationData = getAuthorizationProperty();
            if (null != authorizationData)
                connection.setRequestProperty(authorizationData.getFirst(), authorizationData.getSecond());

            setMethod(connection);

            addParameters(connection, parameters);

            responseCode = connection.getResponseCode();

            BufferedReader buffer;
            if (null==connection.getErrorStream())
                buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            else
                buffer = new BufferedReader(new InputStreamReader(connection.getErrorStream()));

            if (null!=buffer) {
                String inputLine;
                while ((inputLine = buffer.readLine()) != null) {
                    responseData.append(inputLine);
                }
                buffer.close();
            }

        } catch (Exception error) {
            return result.setError(error.toString());
        } finally {
            if (null!=connection)
                connection.disconnect();
        }

        return result.setOk(new Pair<>(responseCode, responseData.toString()));
    }
}
