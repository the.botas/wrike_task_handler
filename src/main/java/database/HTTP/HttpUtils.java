/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.HTTP;

import com.google.gson.*;
import com.intellij.openapi.util.Pair;
import utils.Result;

import javax.net.ssl.HttpsURLConnection;
import java.util.ArrayList;

public class HttpUtils {

    /**
     * @param responseData the http response string to be converted
     * @return
     * On success: a json object containing the data from the http response <br>
     * On error: null
     */
    public static JsonObject httpResponseToJsonObject(String responseData) {
        if (null==responseData) return null;

        JsonObject outputData;
        try {
            outputData = new GsonBuilder().create().fromJson(responseData, JsonObject.class);
        } catch (Exception ignore) {
            return null;
        }

        return outputData;
    }

    /**
     * @param request the http request object that will be used to send the request to the database
     * @param path the path to be used by the http request
     * @param parameter the http request parameter
     * @return
     * On success: a Result object containing in the valid value the response to the http request <br>
     * On error: a Result object containing in the error value a string that describes the error that occurred
     */
    public static Result<String> getResponse (HttpRequest request, String path, String parameter) {
        Result<String> result = new Result<>();

        if (null==request)
            return result.setError("Http request object can not be null!");

        if (null==path)
            return result.setError("Path can not be null!");

        Result<Pair<Integer,String>> response = request.getResponse(path, parameter);
        if (null==response)
            return result.setError("Unknown error occurred while attempting to get response from database.");

        if (response.isError())
            return result.setError(response.getError());

        Integer responseCode = response.getOk().getFirst();
        if (!isInPositiveResponseRange(responseCode)) {
            return result.setError("Negative response: " + responseCode + ": " +response.getOk().getSecond() + " received from database");
        }

        return result.setOk(response.getOk().getSecond());
    }

    private static boolean isInPositiveResponseRange (Integer responseCode) {
        if (null==responseCode)
            return false;

        if ( responseCode < HttpsURLConnection.HTTP_OK )
            return false;

        return HttpsURLConnection.HTTP_BAD_REQUEST > responseCode;
    }

    /**
     * @param responseData the http response as a string
     * @return
     * On success: a Result object that contains in the valid value a list of json objects read from the data tag of the response <br>
     * On error: a Result object that contains in the error value a string describing the error that occurred
     */
    public static Result<ArrayList<JsonObject>> getDataArrayFromHttpResponse (String responseData) {
        Result<ArrayList<JsonObject>> result = new Result<>();

        if (null==responseData)
            return result.setError("http response data is null!");

        JsonObject responseContentAsJson = HttpUtils.httpResponseToJsonObject(responseData);
        if (null==responseContentAsJson)
            return result.setError("http response data is not a valid json object!");

        String dataMember = "data";
        if (!responseContentAsJson.has(dataMember)) {
            return result.setError("Source jsonObject has no field named: "+dataMember);
        }

        JsonArray data;
        try {
            data = responseContentAsJson.get(dataMember).getAsJsonArray();
        } catch (Exception error) {
            return result.setError("Exception occurred while interpreting http response data object as list!");
        }

        if (null==data) {
            return result.setError("Could not interpret http response data object as list!");
        }

        ArrayList<JsonObject> objects = new ArrayList<>();
        for (JsonElement informationElement: data) {
            JsonObject informationObject;
            try {
                informationObject = informationElement.getAsJsonObject();
            } catch (Exception error) {
                result.setError("Exception occurred while interpreting some json object in http response data list.");
                continue;
            }
            if (null!=informationObject)
                objects.add(informationObject);
        }

        return result.setOk(objects);
    }
}
