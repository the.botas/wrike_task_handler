/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.HTTP;

import com.intellij.openapi.util.Pair;
import com.intellij.util.net.HTTPMethod;
import utils.Result;

import javax.net.ssl.HttpsURLConnection;
import java.io.OutputStream;

public class HttpPost extends HttpRequest {
    private String _baseAddress;
    private Pair<String,String> _authorizationHeader;

    /**
     * @return the http address of the database
     */
    protected String baseAddress() {
        return _baseAddress;
    }

    /**
     * @return the authorization header as a pair of strings
     */
    protected Pair<String,String> getAuthorizationProperty() {
        return _authorizationHeader;
    }

    /**
     * @param connection the connection object
     * @param parameters the parameters to be added to the connection object
     */
    protected void addParameters (HttpsURLConnection connection, String parameters) throws Exception {
        if (null==connection) return;
        if (null==parameters) return;
        connection.setDoOutput(true);
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(parameters.getBytes());
        outputStream.flush();
        outputStream.close();
    }

    /**
     * @param connection the connection object to be set to POST method
     */
    protected void setMethod(HttpsURLConnection connection) throws Exception {
        if (null==connection) return;
        connection.setRequestMethod(HTTPMethod.POST.name());
    }

    public HttpPost(String baseAddress, Pair<String, String> authorizationHeader) {
        _baseAddress = baseAddress;
        _authorizationHeader = authorizationHeader;
    }

    /**
     * Same as the getResponse from the HttpRequest class
     */
    public Result<Pair<Integer,String>> getResponse (String path, String parameters) {
        if (null==path)
            return new Result<Pair<Integer,String>>().setError("Path can not be null!");
        if (null==parameters)
            parameters="";
        return super.getResponse(path, parameters);
    }
}
