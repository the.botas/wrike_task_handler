/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.Models;

import com.google.gson.JsonObject;

public class WrikeUser {

    private String uid = "";
    private String firstName = "";
    private String lastName = "";
    private Boolean isMe = false;

    /**
     * Field names in expected in the Json Object received from the database
     */
    protected final class JsonField {
        static final String uid = "id";
        static final String type = "type";
        static final String personType = "Person";
        static final String firstName = "firstName";
        static final String lastName = "lastName";
        static final String me = "me";
        static final String isMe = "true";
    }

    /**
     * Reads user data from json object. If the ID and type fields are not present in the json object an error will be returned.
     * @param jsonObject input json object that contains the user data.
     * @return
     * On success: null
     * On error: a string describing the error that occurred
     */
    public String fromJsonObject(JsonObject jsonObject) {
        if (jsonObject == null) {
            return "Source jsonObject for user description can not be null!";
        }

        if (!jsonObject.has(JsonField.type)) {
            return "Source jsonObject has no field: "+JsonField.type;
        } else {
            String type = jsonObject.get(JsonField.type).getAsString();
            if (!type.equals(JsonField.personType))
                return null;
        }

        if (!jsonObject.has(JsonField.uid)) {
            return "Source jsonObject has no field: " + JsonField.uid;
        } else {
            uid = jsonObject.get(JsonField.uid).getAsString();
        }

        if (jsonObject.has(JsonField.firstName)) {
            firstName = jsonObject.get(JsonField.firstName).getAsString();
        }

        if (jsonObject.has(JsonField.lastName)) {
            lastName = jsonObject.get(JsonField.lastName).getAsString();
        }

        if (jsonObject.has(JsonField.me)) {
            String meValue = jsonObject.get(JsonField.me).getAsString();
            if (meValue.equals(JsonField.isMe))
                isMe=true;
        }

        return null;
    }

    /**
     * @return a string containing the first and last name of the stored user
     */
    public String getName () {
        return firstName+" "+lastName;
    }

    /**
     * @return
     * True if the current stored user is the user who used the token for the http request
     * False for all other users
     */
    public Boolean isMe () {
        return isMe;
    }

    /**
     * @return a string with the stored user ID
     */
    public String getId () {
        return uid;
    }
}
