/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.Models;

import com.google.gson.JsonObject;

public class WrikeProject {
    private String uid = "";
    private String title = "";

    private final class JsonField {
        static final String id = "id";
        static final String title = "title";
        static final String scope = "scope";
        static final String WsFolderType = "WsFolder";
    }

    /**
     * @return the currently stored id of the project
     */
    public String getId() {
        return uid;
    }

    /**
     * @return the currently stored title of the project
     */
    public String getTitle() {
        return title;
    }

    /**
     * Reads project data from json object. If the ID and scope fields are not present in the json object an error will be returned.
     * @param jsonObject input json object that contains the project data.
     * @return
     * On success: null <br>
     * On error: a string describing the error that occurred
     */
    public String fromJsonObject(JsonObject jsonObject) {
        if (jsonObject == null) {
            return "Source jsonObject for project description can not be null!";
        }

        if (!jsonObject.has(JsonField.scope)) {
            return "Source jsonObject has no field: "+ JsonField.scope;
        } else {
            String type = jsonObject.get(JsonField.scope).getAsString();
            if (!type.equals(JsonField.WsFolderType))
                return null;
        }

        if (!jsonObject.has(JsonField.id)) {
            return "Source jsonObject has no field: " + JsonField.id;
        } else {
            uid = jsonObject.get(JsonField.id).getAsString();
        }

        if (jsonObject.has(JsonField.title)) {
            title = jsonObject.get(JsonField.title).getAsString();
        }

        return null;
    }
}
