/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database.Models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import utils.Result;
import utils.StringOperations;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WrikeTask {
    private final class Tag {
        static final String description = "description";
        static final String title = "title";
        static final String status = "status";
        static final String responsible = "responsibles";
    }

    private final class JsonField {
        static final String id = "id";
        static final String title = "title";
        static final String description = "description";
        static final String status = "status";
        static final String responsibles = "responsibleIds";
    }

    private String uid = "";
    static final String uidDelimiter = "WrikeID_";
    private String title = "";
    private String description = "";
    private String status = "";
    private String responsible = "";

    /**
     * Contains all the valid task status values and a function that tests if a string is one of the valid status values
     */
    public static final class Status {
        public static final String active = "Active";
        public static final String completed = "Completed";
        public static final String deferred = "Deferred";
        public static final String canceled = "Cancelled";

        static boolean isValidValue(String value) {
            if (null==value) return false;
            return ( value.equals(active) || value.equals(completed) || value.equals(deferred) || value.equals(canceled));
        }
    }

    /**
     * @param text a string containing the wrike task id preceded by the id delimiter
     * @return
     * On success: null <br>
     * On failure: a string describing the error
     */
    public String addIdFromDelimitedText(String text) {
        if (null==text) {
            return "Text with ID can not be null!";
        }

        ArrayList<String> foundIds = getTaskIdValues(text);

        if (foundIds.size()==0) {
            return "Selected text does not contain task id field.";
        }

        if (foundIds.size()>1) {
            return "Selected text contains multiple task id fields.";
        }

        uid = foundIds.get(0);
        return null;
    }

    private ArrayList<String> getTaskIdValues(String text) {
        if (null==text)
            return new ArrayList<>();

        ArrayList<String> sections = new ArrayList<>(StringOperations.splitToListByDelimiter(text, uidDelimiter));

        if (1>=sections.size())
            return new ArrayList<>();
        sections.remove(0);

        List<String> lines = sections.stream().filter( s -> !s.isEmpty() )
                .map(s -> StringOperations.splitToListByDelimiter(s, StringOperations.newLineDelimiter).get(0) )
                .collect(Collectors.toList());

        List<String> words = lines.stream().filter( s -> !s.isEmpty() )
                .map(s -> StringOperations.splitToListByDelimiter(s, " ").get(0) )
                .collect(Collectors.toList());

        List<String> alfaNumericValues = words.stream().filter( s -> !s.isEmpty() )
                .map(s -> StringOperations.splitToListByDelimiter(s, "\\P{LD}+").get(0).trim() )
                .collect(Collectors.toList());

        return alfaNumericValues.stream().filter(s -> !s.isEmpty()).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @param newTitle the new title of the task
     * @return
     * On success: null <br>
     * On failure: a string describing the error
     */
    public String addTitle(String newTitle) {
        if (null==newTitle)
            return "Task title can not be null!";

        String titleCandidate = newTitle.trim();
        if (titleCandidate.isEmpty())
            return "Task title can not be empty!";

        title = titleCandidate;
        return null;
    }

    /**
     * @param newStatus the new status of the task. If the string is not one of the valid status values an error will be returned.
     * @return
     * On success: null <br>
     * On failure: a string describing the error
     */
    public String setStatus(String newStatus) {
        if (Status.isValidValue(newStatus)) {
            status = newStatus;
            return null;
        }
        return "Invalid status value!";
    }

    /**
     * @param newResponsible the id of the new task responsible
     * @return
     * On success: null <br>
     * On failure: a string describing the error
     */
    public String setResponsible (String newResponsible) {
        if (null==newResponsible)
            return "Responsible ID can not be null!";
        if (newResponsible.trim().isEmpty())
            return "Responsible ID can not be empty!";
        responsible=newResponsible;
        return null;
    }

    /**
     * Reads task data from json object.
     * @param jsonObject input json object that contains the task data.
     * @return
     * On success: null <br>
     * On failure: a string describing the error
     */
    public String fromJsonObject(JsonObject jsonObject) {
        if (jsonObject == null) {
            return"Source jsonObject for task description can not be null!";
        }

        String dataMember = "data";
        if (!jsonObject.has(dataMember)) {
            return "Source jsonObject for task description has not filed named: "+dataMember;
        }

        JsonObject projectInformationObject;
        try {
            JsonArray data = jsonObject.get(dataMember).getAsJsonArray();
            projectInformationObject = data.get(0).getAsJsonObject();
        } catch (Exception error) {
            return "Exception while parsing jsonObject for task description: "+error.toString();
        }

        if (null==projectInformationObject) {
            return "Source jsonObject for task description is missing project information data";
        }

        if (projectInformationObject.has(JsonField.id)) {
            uid = projectInformationObject.get(JsonField.id).getAsString();
        }

        if (projectInformationObject.has(JsonField.title)) {
            title = projectInformationObject.get(JsonField.title).getAsString();
        }

        if (projectInformationObject.has(JsonField.description)) {
            description = projectInformationObject.get(JsonField.description).getAsString().
                    replace(StringOperations.htmlLineBreak, StringOperations.newLineDelimiter);
        }

        if (projectInformationObject.has(JsonField.status)) {
            setStatus(projectInformationObject.get(JsonField.status).getAsString());
        }

        if (projectInformationObject.has(JsonField.responsibles)) {
            JsonArray ids=null;
            try {
                ids = projectInformationObject.get(JsonField.responsibles).getAsJsonArray();
            } catch (Exception ignored) {}

            if (null!=ids) {
                if(ids.size()>1)
                    responsible=ids.get(0).getAsString();
            }
        }

        return null;
    }

    /**
     * @param text a string that will be used to construct the new task <br>
     *             the full text will be added to the task description <br>
     *             the first line of the text will be used to construct the task title <br>
     * @return
     * On success: null <br>
     * On failure: a string describing the error
     */
    public String fromText (String text) {
        
        if (null==text)
            return "Source text for task can not be null!";
        if (text.trim().isEmpty())
            return "Source text for task can not be empty!";
        if(0<getTaskIdValues(text).size())
            return "Selected text already contains a task Id!";

        title = StringOperations.getCleanedFirstValidLine(text);
        description = text;
        return null;
    }

    /**
     * @return a standard string used as a marker for wrike id field followed by the wrike id.
     */
    public String getNoteWithId() {
        return " "+uidDelimiter+uid+" ";
    }

    /**
     * @return the title, description, status and responsible fields of the task as a http parameter string
     */
    public String asParameterString() {
        String output = "";

        if (!title.isEmpty()) {
            Result<String> result = StringOperations.replaceUrlEscapeChars(title);
            if (result.isOk())
                output = addParameter(output, Tag.title, result.getOk() );
        }

        if (!description.isEmpty()) {
            Result<String> result = StringOperations.replaceUrlEscapeChars( description.replace(StringOperations.newLineDelimiter, StringOperations.htmlLineBreak) );
            if (result.isOk())
                output = addParameter(output, Tag.description, result.getOk() );
        }

        if (!status.isEmpty()) {
            output = statusAsParameterString(output);
        }

        if (!responsible.isEmpty()) {
            output = addParameter(output, Tag.responsible, "[\""+responsible+"\"]");
        }

        return output;
    }

    /**
     * @param initialData initial http parameter value. Can be an empty string if not previous parameter value is used
     * @return the status of the task as a http parameter string
     */
    public String statusAsParameterString(String initialData) {
        if (null==initialData)
            initialData = "";
        return addParameter(initialData, Tag.status, status);
    }

    private String addParameter (String initialData, String tag, String value) {

        if (null==initialData) initialData="";
        if (null==tag)  tag="";
        if (null==value)  value="";

        if (!initialData.isEmpty()) {
            initialData = initialData.concat("&");
        }
        return initialData.concat(tag).concat("=").concat(value);
    }

    /**
     * @return a string with the id of the task
     */
    public String getId(){
        return uid;
    }

    /**
     * @return a string with the title of the task
     */
    public String getTitle() { return this.title; }

    /**
     * @return a string with the status of the task
     */
    public String getStatus() {
        return status;
    }

    String getDescription() {
        return description;
    }

    String getResponsible() {
        return responsible;
    }

}
