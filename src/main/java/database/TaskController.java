/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database;

import com.google.gson.JsonObject;
import com.intellij.openapi.util.Pair;
import database.HTTP.HttpPost;
import database.HTTP.HttpPut;
import database.HTTP.HttpUtils;
import database.Models.WrikeTask;
import host.Identity;
import utils.Result;

public class TaskController {
    private Identity identity;

    public TaskController(Identity connectionIdentity) {
        if (null==connectionIdentity) return;
        identity = connectionIdentity;
    }

    /**
     * @param description a string that will be added to the task description
     * @param title a string that will be the title of the new task
     * @return
     * On success: a Result object that contains in the valid value a pair of string representing the task title and task id in the Wrike database
     * On failure: a Result object that contains in the error value a string that describes the error that occurred
     */
    public Result<Pair<String,String>> createTaskFromString (String description, String title) {
        Result<Pair<String,String>> result = new Result<>();

        if (null==identity) return result.setError("Identity data for HTTP not configured!");
        if (null==description) return result.setError("Source text for task can not be null!");
        if (description.isEmpty()) return result.setError("Source text for task can not be empty!");

        WrikeTask task = new WrikeTask();

        String error = task.fromText(description);
        if (null!=error)
            return result.setError(error);

        task.setResponsible(identity.getUser());

        String newStatus = WrikeTask.Status.active;
        task.setStatus(newStatus);

        if (null!=title) {
            if (!title.isEmpty()) {
                task.addTitle(title);
            }
        }

        if (task.getTitle().isEmpty()) {
            return result.setError("Can not create task with empty title!");
        }

        String wrikeProjectAddress = "folders/" + identity.getProject() +"/tasks";

        Result<String> responseData = HttpUtils.getResponse( new HttpPost(
                ConnectionUtils.baseAddress(),
                ConnectionUtils.getAuthorizationProperty(identity.getToken())
                ), wrikeProjectAddress, task.asParameterString());
        if (responseData.isError()) return result.setError(responseData.getError());

        JsonObject projectsData = HttpUtils.httpResponseToJsonObject(responseData.getOk());
        if (null==projectsData)
            return result.setError("Database response is not a valid json object!");

        String parseError = task.fromJsonObject(projectsData);
        if(null!=parseError) {
            return result.setError(parseError);
        }

        return result.setOk(new  Pair<>(task.getTitle(), task.getNoteWithId()));
    }

    /**
     * @param taskText a string that contains only one valid wrike task id
     * @return
     * On success: null <br>
     * On error: a string describing the error that occurred
     */
    public String closeTask(String taskText) {
        return setTaskStatus(WrikeTask.Status.completed, taskText);
    }

    private String setTaskStatus (String newStatus, String taskText) {
        if (null==taskText) return "Source text for task can not be null!";
        if (taskText.isEmpty()) return "Source text for task can not be empty!";
        if (null==identity) return "Identity data for HTTP not configured!";

        WrikeTask task = new WrikeTask();
        String error = task.addIdFromDelimitedText(taskText);
        if (null!=error)
            return error;

        task.setStatus(newStatus);
        if(!task.getStatus().equals(newStatus))
            return "Could not set task status "+newStatus;


        String wrikeTaskAddress = "tasks/" + task.getId();

        Result<String> responseData =  HttpUtils.getResponse( new HttpPut(
                ConnectionUtils.baseAddress(),
                ConnectionUtils.getAuthorizationProperty(identity.getToken())
                ), wrikeTaskAddress, task.statusAsParameterString(""));
        if (responseData.isError()) return responseData.getError();

        JsonObject projectsData = HttpUtils.httpResponseToJsonObject(responseData.getOk());
        if (projectsData == null)
            return "Database response is not a valid json object!";

        String parseError = task.fromJsonObject(projectsData);
        if(null!=parseError) return parseError;

        if (!task.getStatus().equals(newStatus))
            return "Could not set task to: "+newStatus;

        return null;
    }

}
