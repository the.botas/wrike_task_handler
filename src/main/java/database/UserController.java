/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database;

import com.google.gson.JsonObject;
import database.HTTP.HttpGet;
import database.HTTP.HttpUtils;
import database.Models.WrikeUser;
import host.Identity;
import utils.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

public class UserController {

    private Identity identity;

    /**
     * @param connectionIdentity an Identity object that will be used to authorize requests to the database
     */
    public UserController(Identity connectionIdentity) {
        if (null==connectionIdentity) return;
        identity = connectionIdentity;
    }

    /**
     * @return
     * On success: a Result object that contains in the valid value a String with the ID of the user that used the authentication token <br>
     * On error: a Result object that contains in the error value a String describing the error that occurred
     */
    public Result<String> getMyDatabaseId () {
        Result<String> result = new Result<>();

        Result<ArrayList<WrikeUser>> users = getUserObjects();
        if (users.isError()) {
            return result.setError(users.getError());
        }

        WrikeUser me = users.getOk().stream().filter(WrikeUser::isMe).findFirst().orElse(null);
        if(null==me) {
            return result.setError("Could not find definition of own user in database!");
        }

        return result.setOk(me.getId());
    }

    private Result<ArrayList<WrikeUser>> getUserObjects() {
        Result<ArrayList<WrikeUser>> result = new Result<>();

        if (null==identity)
            return result.setError("Identity data for HTTP not configured!");

        Result<String> responseData = HttpUtils.getResponse( new HttpGet(
                ConnectionUtils.baseAddress(),
                ConnectionUtils.getAuthorizationProperty(identity.getToken())
                ),"contacts",null);
        if ( responseData.isError() )
            return result.setError(responseData.getError());

        ArrayList<WrikeUser> users = new ArrayList<>();

        Result<ArrayList<JsonObject>> objects = HttpUtils.getDataArrayFromHttpResponse(responseData.getOk());
        if (objects.isError())
            return result.setError(objects.getError());

        for (JsonObject userData: objects.getOk()) {
            WrikeUser newUser = new WrikeUser();
            newUser.fromJsonObject(userData);
            if (newUser.getId().isEmpty())
                continue;
            users.add(newUser);
        }

        return result.setOk(users);
    }

    /**
     * @return
     * On success: a Result object that contains in the valid value a hash map with the user id and user name. <br>
     * On error: a result object that contains in the error value a string describing the error that occurred.
     */
    public Result<HashMap<String, String>> getUsers() {
        Result<HashMap<String, String>> result = new Result<>();

        Result<ArrayList<WrikeUser>> userObjects = getUserObjects();
        if (userObjects.isError()) return result.setError(userObjects.getError());

        return result.setOk(new HashMap<>(userObjects.getOk().stream().collect(Collectors.toMap(WrikeUser::getId, WrikeUser::getName))));
    }
}
