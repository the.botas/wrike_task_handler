/*
 * MIT License
 *
 * Copyright (c) 2019 Viorel Bota, Marian Petru Bota, Cristina Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package database;

import com.google.gson.JsonObject;
import database.HTTP.HttpGet;
import database.HTTP.HttpUtils;
import database.Models.WrikeProject;
import host.Identity;
import utils.Result;

import java.util.ArrayList;
import java.util.HashMap;

public class ProjectController {

    private Identity identity;

    public ProjectController(Identity connectionIdentity) {
        if (null==connectionIdentity) return;
        identity = connectionIdentity;
    }

    /**
     * @return
     * On success: a Result object that contains in the valid value a hash map of wrike project IDs and wrike project titles <br>
     * On error: a Result object that contains in the error value the string that describes the error that occurred
     */
    public Result<HashMap<String,String>> getProjects() {
        Result<HashMap<String,String>> result = new Result<>();

        if (null==identity)
            return result.setError("Identity data for HTTP not configured!");

        HashMap<String,String> projects = new HashMap<>();

        Result<String> response =  HttpUtils.getResponse( new HttpGet(
                ConnectionUtils.baseAddress(),
                ConnectionUtils.getAuthorizationProperty(identity.getToken())
                ),"folders",null);
        if (response.isError())
            return result.setError(response.getError());

        Result<ArrayList<JsonObject>> objects = HttpUtils.getDataArrayFromHttpResponse(response.getOk());
        if (objects.isError())
            return result.setError(objects.getError());

        for (JsonObject projectData: objects.getOk()) {
            WrikeProject newProject = new WrikeProject();
            newProject.fromJsonObject(projectData);

            if (newProject.getId().isEmpty())
                continue;
            if (newProject.getTitle().isEmpty())
                continue;

            projects.put(newProject.getId(), newProject.getTitle());
        }

        return result.setOk(projects);
    }
}
