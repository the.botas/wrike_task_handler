# WrikeTaskHandler

Wrike Task Handler is a plugin for IntelliJ, designed to help developers working with Wrike as their project management tool.

You can easily create, assign and complete tasks for Wrike without ever leaving the editor. 

If you ever added a TODO or Note in the code, then the Wrike Task Handler is the right tool for you.

Simply select the code section or comment, right click on it and convert it to a task in Wrike.

Having the TODO in Wrike helps teams evaluate and track the effort needed to implement a software component.

TODO notes saved as Wrike tasks allow complex descriptions thus avoiding the risk of forgetting why a TODO note was written for.

The Wrike Task Handler plugin can be a powerful tool for teams when doing code reviews. 
Each review note can be saved as a Wrike task thus leveraging the powerful project management features of Wrike directly from Intellij.

## Release 440 

### Features
- Connect to Wrike database using permanent token
- Select project in which new tasks will be added
- Select user to be assigned as responsible to new tasks
- Create a new task
- Close an existing task
- Status messages are presented in the event log of IntelliJ

You can check out the presentation video on YouTube:
[https://youtu.be/MFCKgAE9Afw](https://youtu.be/MFCKgAE9Afw)

### Requirements
- Intellij Version 2019.1 or higher

### Installation
To install the plugin from the zip file follow these steps:

- Download the latest zip archive from build/distributions: https://gitlab.com/the.botas/wrike_task_handler/tree/release/build/distributions
- Start IntelliJ 
- Go to File -> Settings -> Plugins
- Click on the sprocket icon and select "Install Plugin from disk"
- Select the downloaded archive and click Next 2 times
- Restart IDE

## Connecting to Wrike from Intellij
The plugin connects to the Wrike database using a permanent access token.

- Go to https://www.wrike.com/ log in to your account 
- Go to "Apps and Integrations" (under your name top right corner)
- Click on "API"
- Enter a new name for your token
- Click on "Create New"
- Scroll down to "Permanent access token" and click on "Create v4 Token"
- Select and copy the generated token*
- Open the desired project in IntelliJ
- Open the source file, you wish to work on, in the IntelliJ editor
- Right click anywhere in the IntelliJ editor
- Select the "Wrike" plugin on the menu that pops up***
- Click on "Connect"
- Enter the token copied earlier
- Select one of your projects**

*This token can't be viewed or copied later

**If you cancel the connection process at any step, including the project selection step, the credentials will not be saved on the host system. This is done to protect from overwriting old valid connection data.

***If the "Wrike" option is greyed out it might be because Intellij did not finish loading all components. Please wait a little and retry again.

Once Connected the other operations should be visible on the right click menu -> Wrike.

## Selecting a (new) project
In Wrike each new task has to have a parent project.
Selecting the project to which new tasks will be added can be easily done, so that you can work on multiple projects without having to go back and forth between IntelliJ and Wrike

- Right click on any line in the editor
- Select the "Wrike" plugin on the menu that pops up
- Click on "Select Project"
- Choose a project from the dropdown list and press OK

The connection step should already be completed to be able to select a project.

## Selecting a new responsible
Wrike task can have a user assigned as task responsible.
By default, the responsible for all new created tasks will be the Wrike user who register the token during the connection phase.
You can easily change the responsible that will be assigned to new tasks from the IntelliJ IDE.

- Right click on any code line
- Select the "Wrike" plugin on the menu that pops up
- Click on "Select Responsible"
- Select the responsible to be assigned to all new tasks, then press OK

The connection step should already be completed to be able to select a new project.

## Creating a new task
New tasks take the code from IntelliJ as description so that you can easily view it in Wrike as well.

- Right click on the line of code that you want to use as your new task or select as much code as you want and right click on the selected block.
- Select the "Wrike" plugin on the menu that pops up
- Click on "New task"
- You will be prompted for the title of the new task. 
By default the first line of the selected code is proposed as the task title but you can write your own title.
- If the title is different from the first line of the selected text then the plugin automatically inserts a commented line, above the selected text, with the title and the WrikeID of the new task.
If the title of the task is the same as the first line of the selected text then the wrike ID is added at the end of the first line.

The connection step should already be completed in order to be able to select a new project.

The option "New task" will not be visible if the selected line/text is empty.

Creating a new task using text that already contains a taks ID will be rejected.

## Closing a task
Tasks entered in Wrike can be easily set to closed, straight from IntelliJ so that developers don't need to leave the editor.

- Right click on the line with the WrikeID or select a section of text containing a WrikeID and right click on the selected block.
- Select the "Wrike" plugin on the menu that pops up
- Click on "Close task"
- The selected line/text is deleted from the IntelliJ editor and the task in Wrike moves to status "Completed"

The connection step should already be completed in order to be able to select a new project.

The option "Close task" will not be visible if the selected line/text is empty.

Attempting to close a task by selecting text with multiple task ID will be rejected.

WARINING: the selected text/line will be erased from the editor once the task is closed. You can undo the erase text operation but this will not alter the state of the task in wrike. 

## Uninstall
To uninstall the plugin follow these steps:

- Start IntelliJ 
- Go to File -> Settings -> Pluggins
- Click on the "Wrike" plugin and select Uninstall
- Restart IDE

## Documentation
The code documentation can be accessed at:
https://gitlab.com/the.botas/wrike_task_handler/tree/release/build/docs/javadoc
